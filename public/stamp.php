<?php

// 入力された文字を配列にする。特定の文字数の場合、後に文字を足して字数を調整する
function mbStringToArray($sStr, $sEnc = 'UTF-8')
{

    $aRes = array();

    while ($iLen = mb_strlen($sStr, $sEnc)) {
        $aRes[] = mb_substr($sStr, 0, 1, $sEnc);
        $sStr   = mb_substr($sStr, 1, $iLen, $sEnc);
    }

    // 追記文字
    switch (count($aRes)) {
        case 5:
        case 8:
        case 11:
        case 15:
        case 19:
            $aRes[] = "印";
            break;
        case 7:
        case 10:
        case 14:
        case 18:
            $aRes[] = "之";
            $aRes[] = "印";
            break;
        case 13:
        case 17:
            $aRes[] = "之";
            $aRes[] = "匠";
            $aRes[] = "印";
            break;
    }

    return $aRes;
}

$name       = 'こんにちは';
$shape      = 2;

mb_regex_encoding("UTF-8");

$name_ary = mbStringToArray($name);

//文字数
$str_count = count($name_ary);


/* 位置調整
		 * $chr = [
		 *     size:フォントサイズ
		 *     x, y:各文字のx位置
		 *     ex, ey: 伸ばし棒文字の位置補正
		 * ]
		 * $frame = [
		 *     size:フレームのフォントサイズ
		 *     x, y:フレームの位置
		 * ]
		 */
switch ($str_count) {

    case 1:
        $chr = [
            'size' => 34,
            'x' => [23],
            'y' => [52],
            'ex' => 7,
            'ey' => -39
        ];
        $frame = [
            'size' => 70,
            'x' => 0,
            'y' => 11,
        ];
        break;

    case 2:
        $chr = [
            'size' => 28,
            'x' => [34, 34],
            'y' => [36, 72],
            'ex' => 6,
            'ey' => -32
        ];
        $frame = [
            'size' => 80,
            'x' => -2,
            'y' => 11,
        ];
        break;

    case 3:
        $chr = [
            'size' => 22,
            'x' => [43, 43, 43],
            'y' => [27, 55, 83],
            'ex' => 4,
            'ey' => -25
        ];
        $frame = [
            'size' => 88,
            'x' => -3,
            'y' => 14,
        ];
        break;

    case 4:
        $chr = [
            'size' => 24,
            'x' => [58, 58, 26, 26],
            'y' => [38, 72, 38, 72],
            'ex' => 4,
            'ey' => -28
        ];
        $frame = [
            'size' => 86,
            'x' => -3,
            'y' => 14,
        ];
        break;

    case 6:
        $chr = [
            'size' => 20,
            'x' => [60, 60, 60, 28, 28, 28],
            'y' => [28, 54, 80, 28, 54, 80],
            'ex' => 3,
            'ey' => -23
        ];
        $frame = [
            'size' => 88,
            'x' => -3,
            'y' => 14,
        ];
        break;

    case 9:
        $chr = [
            'size' => 18,
            'x' => [74, 74, 74, 50, 50, 50, 26, 26, 26],
            'y' => [30, 55, 80, 30, 55, 80, 30, 55, 80],
            'ex' => 3,
            'ey' => -21
        ];
        $frame = [
            'size' => 94,
            'x' => -3,
            'y' => 14,
        ];
        break;

    case 12:
        $chr = [
            'size' => 15,
            'x' => [76, 76, 76, 76, 53, 53, 53, 53, 30, 30, 30, 30],
            'y' => [23, 44, 65, 86, 23, 44, 65, 86, 23, 44, 65, 86],
            'ex' => 3,
            'ey' => -17
        ];
        $frame = [
            'size' => 94,
            'x' => -3,
            'y' => 14,
        ];
        break;

    case 16:
        $chr = [
            'size' => 15,
            'x' => [85, 85, 85, 85, 66, 66, 66, 66, 47, 47, 47, 47, 28, 28, 28, 28],
            'y' => [26, 46, 66, 86, 26, 46, 66, 86, 26, 46, 66, 86, 26, 46, 66, 86],
            'ex' => 3,
            'ey' => -17
        ];
        $frame = [
            'size' => 98,
            'x' => -3,
            'y' => 14,
        ];
        break;

    case 20:
        $chr = [
            'size' => 14,
            'x' => [91, 91, 91, 91, 91, 71, 71, 71, 71, 71, 51, 51, 51, 51, 51, 31, 31, 31, 31, 31],
            'y' => [23, 41, 59, 77, 95, 23, 41, 59, 77, 95, 23, 41, 59, 77, 95, 23, 41, 59, 77, 95],
            'ex' => 2,
            'ey' => -17
        ];
        $frame = [
            'size' => 106,
            'x' => -3,
            'y' => 14,
        ];
        break;

    default:
        $chr = [
            'size' => 0,
            'x' => [],
            'y' => [],
            'ex' => 0,
            'ey' => 0
        ];
        $frame = [
            'size' => 0,
            'x' => -0,
            'y' => 0,
        ];
}

$frame_base_x = 0;
$frame_base_y = $frame['size'];



putenv('GDFONTPATH=' . realpath('.'));

$frame_font = [
    1 => './fonts/Shippori_Mincho/ShipporiMincho-Regular.ttf', // 太い
    2 => './fonts/ipamp.ttf' //細い
];

if ($str_count >= 9 && $shape == 3) {
    $frame_type = 2;
} else {
    $frame_type = 1;
}

$stamp_frame = $frame_font[$frame_type];

// フォント
$font        = './fonts/Shippori_Mincho/ShipporiMincho-Bold.ttf';

// 枠定義
$frame_shape = [1 => ' ', 2 => '□', 3 => '○'];
$x = $frame_base_x + $frame['x'];
$y = $frame_base_y + $frame['y'];


// 大きさを測定
$result = ImageTTFBBox($frame['size'], 0, $font, $frame_shape[$shape]);

// 左上
$x0 = $result[6];
$y0 = $result[7];
// 右下
$x1 = $result[2];
$y1 = $result[3];

// 幅と高さを取得
$width  = $x1 - $x0;
$height = $y1 - $y0;

if ($width >= $height) {
    $size = $width;
} else {
    $size = $height;
}

//echo $size;
// イメージリソースを生成
$img = imagecreatetruecolor($size, $size);

// 色
$red   = imagecolorallocate($img, 255, 0, 0);
$white = imagecolorallocate($img, 255, 255, 255);


// 背景を塗りつぶす
imagefilledrectangle($img, 0, 0, $size, $size, $white);


if ($shape != 1 && $str_count <= 20) {

    // 枠表示
    ImageTTFText(
        $img,
        $frame['size'],
        0,
        $x,
        $y,
        $red,
        $stamp_frame,
        $frame_shape[$shape]
    );

    // 文字表示
    foreach ($name_ary as $key => $val) {

        $rotate = 0;
        $ex = 0;
        $ey = 0;

        // 伸ばし棒の場合の補正処理
        if ($val == 'ー') {
            $rotate = 270;
            $ex =  $chr['ex'];
            $ey =  $chr['ey'];
        }
        ImageTTFText(
            $img,
            $chr['size'],
            $rotate,
            $chr['x'][$key] + $frame['x'] + $ex,
            $chr['y'][$key] + $frame['y'] + $ey,
            $red,
            $font,
            $val
        );
    }
}

header('Content-Type: image/png');

// PNGとして出力
ImagePNG($img);
imagedestroy($img);

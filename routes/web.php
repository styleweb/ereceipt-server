<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\GuestDocumentController;
use App\Http\Controllers\StampController;
use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/pdf/{document}/{uuid}', [DocumentController::class, 'pdf']);
Route::get('/guest/{document}/{uuid}', [GuestDocumentController::class, 'public']);
Route::get('/public/{document}/{uuid}', [DocumentController::class, 'public']);
Route::get('/stamp/{shape}/{name}', [StampController::class, 'index']);

Route::post('/forgot-password', function (Request $request) {

    // Log::debug($request);
    $request->validate(['email' => 'required|email']);

    $status = Password::sendResetLink(
        $request->only('email')
    );

    return $status === Password::RESET_LINK_SENT
        ? response()->json(['message' => 'sent.'], 200)
        : back()->withErrors(['email' => __($status)]);

    // return $status === Password::RESET_LINK_SENT
    //     ? back()->with(['status' => __($status)])
    //     : back()->withErrors(['email' => __($status)]);
})->middleware('guest')->name('password.email');

// Route::get('/reset-password/{token}', function ($token) {
//     return view('auth.reset-password', ['token' => $token]);
// })->middleware('guest')->name('password.reset');

Route::post('/reset-password', function (Request $request) {
    $request->validate([
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:8|confirmed',
    ]);

    $status = Password::reset(
        $request->only('email', 'password', 'password_confirmation', 'token'),
        function ($user, $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));

            $user->save();

            event(new PasswordReset($user));
        }
    );

    return $status === Password::PASSWORD_RESET
        ? response()->json(['message' => 'Changed.'], 200)
        : response()->json(['message' => 'Error.'], 401);

    // return $status === Password::PASSWORD_RESET
    //     ? redirect()->route('login')->with('status', __($status))
    //     : back()->withErrors(['email' => [__($status)]]);
})->middleware('guest')->name('password.update');


Route::middleware('auth:sanctum')->group(
    function () {
        Route::get('/files/{file_name}', function (Request $request, $file_name) {

            // Log::debug($request->user());
            // $userId = $request->user()->id; ログインuserIDを取得
            $file = Storage::get("{$file_name}");
            $mimeType = Storage::mimeType("{$file_name}");

            return response()->make($file, 200, [
                'Content-Type'        => $mimeType,
            ]);
        });
    }
);

Route::get('/file/{id}', [FileController::class, 'show']);
// Route::get('/csv/receipt', [ReceiptController::class, 'csv']);

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\DocumentTemplateController;
use App\Http\Controllers\GuestDocumentController;

use App\Http\Controllers\ContractController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\MailTemplateController;

use App\Http\Controllers\GroupController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\CustomerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/guest_document', [GuestDocumentController::class, 'store']);
// Route::post('/payjp-webhook', [UserController::class, 'changeStatus']);
// Route::get('/invoice/replace_number/{number?}', [DocumentController::class, 'replace_number']);
// Route::get('/invoice/{uuid}', [DocumentController::class, 'show']);

Route::get('/message/{uuid}', [MailController::class, 'message']);

Route::middleware('auth:sanctum')->group(function () {

    Route::post('/file/message_file_upload', [FileController::class, 'message_file_upload']);

    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::put('/user', [UserController::class, 'update']);

    Route::get('/customer/list', [CustomerController::class, 'list']);
    Route::post('/customer/import', [CustomerController::class, 'importCsv']);

    Route::get('/template/list', [DocumentTemplateController::class, 'list']);
    Route::get('/template/{type}', [DocumentTemplateController::class, 'index']);

    Route::get('/mail_template/list', [MailTemplateController::class, 'list']);
    Route::get('/mail_template/{type}', [MailTemplateController::class, 'index']);

    Route::apiResource('group', GroupController::class)->except([
        'index'
    ]);
    Route::apiResource('template', DocumentTemplateController::class)->except([
        'index'
    ]);
    Route::apiResource('mail_template', MailTemplateController::class)->except([
        'index'
    ]);
    Route::apiResources([
        'estimate' => DocumentController::class,
        'delivery' => DocumentController::class,
        'invoice' => DocumentController::class,
        'receipt' => DocumentController::class,
        'contract' => ContractController::class,
        'mail' => MailController::class,
        'file' => FileController::class,
        'customer' => CustomerController::class,
        // 'estimate_template' => EstimateTemplateController::class,
        // 'delivery_template' => DeliveryTemplateController::class,
        // 'receipt_template' => ReceiptTemplateController::class,
        // 'state' => StateController::class,
        // 'credit/customer' => CreditCustomerController::class,
        // 'credit/card' => CardController::class,
    ]);


    Route::get('/member', [UserController::class, 'index']);
    Route::post('/member', [UserController::class, 'store']);
    Route::get('/member/show', [UserController::class, 'show']);
    Route::delete('/member/{id}', [UserController::class, 'destroy']);

    Route::post('/file/multi_delete', [fileController::class, 'multiDelete']);

    // Route::put('/group/save_template_default', [GroupController::class, 'saveTemplateDefault']);
    // Route::get('/estimate_template', [EstimateTemplateController::class, 'show']);
    // Route::put('/estimate/copy', [EstimateController::class, 'copy']);
    // Route::get('/estimate/get_template/{id?}', [EstimateController::class, 'get_template']);
    // Route::get('/estimate/get_invoice/{id}', [EstimateController::class, 'get_estimate']);
    // Route::get('/invoice/get_copy/{id}', [DocumentController::class, 'get_copy']);
    // Route::get('/invoice/get_template/{id?}', [DocumentController::class, 'get_template']);
    // Route::get('/invoice/from_estimate/{id}', [DocumentController::class, 'from_estimate']);
    // Route::get('/delivery/from_estimate/{id}', [DeliveryController::class, 'from_estimate']);
    // Route::get('/estimate_template/get_group', [EstimateTemplateController::class, 'get_group']);
    // Route::get('/estimate_template/get_copy/{id}', [EstimateTemplateController::class, 'get_copy']);
    // Route::get('/invoice_template/get_group', [DocumentTemplateController::class, 'get_group']);
    // Route::get('/invoice_template/get_copy/{id}', [DocumentTemplateController::class, 'get_copy']);
    // Route::get('/receipt/from_invoice/{id}', [ReceiptController::class, 'from_invoice']);

    // Route::post('/credit/subscription', [SubscriptionController::class, 'store']);
    // Route::get('/credit/plan', [PlanController::class, 'index']);
    // Route::put('/credit/subscription', [SubscriptionController::class, 'update']);
    // Route::delete('/credit/subscription', [SubscriptionController::class, 'cancel']);
    // Route::get('/credit/subscription/resume', [SubscriptionController::class, 'resume']);
    // Route::get('/credit/subscription/cancel_next', [SubscriptionController::class, 'cancelNextCycle']);
});

<?php

namespace App\Http\Controllers\Credit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\Group;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // payjp から顧客を取得
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$user->group->credit_customer_id}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if (empty($user->group->credit_customer_id)) {
            // 顧客がなければ、顧客も登録
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.pay.jp/v1/customers');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
            curl_setopt($ch, CURLOPT_POSTFIELDS, "card={$request->token}");
            $response = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($response);
            Group::where('id', $user->group->id)->update(['credit_customer_id' => $result->id]);
        } else {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$user->group->credit_customer_id}/cards");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
            curl_setopt($ch, CURLOPT_POSTFIELDS, "card={$request->token}");
            $response = curl_exec($ch);
            curl_close($ch);
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $card_id)
    {
        $user = Auth::user();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$user->group->credit_customer_id}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "default_card={$card_id}");
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$user->group->credit_customer_id}/cards/{$id}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        $response = curl_exec($ch);
        curl_close($ch);
    }


    /**
     * Upgrade plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upgrade(Request $request)
    {
        $user = Auth::user();

        //定期購入を追加
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pay.jp/v1/subscriptions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'plan=' . $request->plan . '&customer=' . $user->group->credit_customer_id);
        $response = curl_exec($ch);
        curl_close($ch);

        // $item = User::find(Auth::id());
        // $item->role = $request->plan;
        // $item->save();
    }


    /**
     * Upgrade plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request)
    {
        $user = Auth::user();

        // キャンセル申請
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/subscriptions/{$request->subscription_id}/cancel");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePlan(Request $request)
    {
        $user = Auth::user();

        // 現在の定期購入がある場合は現在の定期購入をキャンセル

        if ($request->plan == 0) {
            // 定期購入がある場合キャンセル
            if (!empty($request->subscription_id)) {
            }
        } else {
            // 定期購入がある場合削除で日割り請求
            if (!empty($request->subscription_id)) {
            }


            // $result = json_decode($response);
            // Log::debug($response);
            // if (!empty($result->error->code)) {
            // }

            // return $response;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pay(Request $request)
    {
        // $id = Auth::id();
        // $plans = [1 => 'standard', 2 => 'premium'];
        // if (isset($plans[$request->plan])) {
        //     $role = $request->plan;
        //     $plan = $plans[$request->plan];
        // } else {
        //     return ['error'];
        // }

        // // 定期課金
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, 'https://api.pay.jp/v1/subscriptions');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        // curl_setopt($ch, CURLOPT_HTTPHEADER, [
        //     'Content-Type' => 'application/x-www-form-urlencoded',
        // ]);
        // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 'plan=' . $plan . '&customer=' . $id);
        // $response = curl_exec($ch);
        // curl_close($ch);

        // Log::debug($response);
        // return $response;
    }
}

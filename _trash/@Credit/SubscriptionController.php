<?php

namespace App\Http\Controllers\Credit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // payjp から顧客を取得
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$user->group->credit_customer_id}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        //定期購入を追加
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pay.jp/v1/subscriptions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'plan=' . $request->plan . '&customer=' . $user->group->credit_customer_id);
        $response = curl_exec($ch);
        curl_close($ch);

        // $item = User::find(Auth::id());
        // $item->role = $request->plan;
        // $item->save();

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        // Has subscription?
        if ($user->group->Credit['subscriptions']['count'] > 0) {
            $current_plan = $user->group->Credit['subscriptions']['data'][0]->plan->id;
            // upgrad or downgrade
            if ($request->plan > $current_plan) {
                return $this->upgrade($request);
            } else {
                return $this->downgrade($request);
            }
        } else {
            // add
            return $this->store($request);
        }
    }

    /**
     * Upgrade plan.
     * 即時切替。日割り請求
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upgrade(Request $request)
    {
        $user = Auth::user();

        $subscription_id = isset($user->group->Credit['subscriptions']['data'][0]->id) ? $user->group->Credit['subscriptions']['data'][0]->id : false;

        if ($subscription_id) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/subscriptions/{$subscription_id}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
            curl_setopt($ch, CURLOPT_POSTFIELDS, "plan={$request->plan}&prorate=true");
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }

    /**
     * Downgrade plan.
     * 次サイクルで切り替え
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downgrade(Request $request)
    {
        $user = Auth::user();

        $subscription_id = isset($user->group->Credit['subscriptions']['data'][0]->id) ? $user->group->Credit['subscriptions']['data'][0]->id : false;

        if ($subscription_id) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/subscriptions/{$subscription_id}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
            curl_setopt($ch, CURLOPT_POSTFIELDS, "next_cycle_plan={$request->plan}");
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }

    public function cancelNextCycle(Request $request)
    {
        $user = Auth::user();

        $subscription_id = isset($user->group->Credit['subscriptions']['data'][0]->id) ? $user->group->Credit['subscriptions']['data'][0]->id : false;

        if ($subscription_id) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/subscriptions/{$subscription_id}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
            curl_setopt($ch, CURLOPT_POSTFIELDS, "next_cycle_plan=");
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }

    /**
     * Upgrade plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request)
    {
        $user = Auth::user();
        $subscription_id = isset($user->group->Credit['subscriptions']['data'][0]->id) ? $user->group->Credit['subscriptions']['data'][0]->id : false;

        // キャンセル
        if ($subscription_id) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/subscriptions/{$subscription_id}/cancel");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }

    /**
     * Resume subscription.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resume()
    {
        $user = Auth::user();
        $subscription_id = isset($user->group->Credit['subscriptions']['data'][0]->id) ? $user->group->Credit['subscriptions']['data'][0]->id : false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/subscriptions/{$subscription_id}/resume");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$user->group->credit_customer_id}/cards/{$id}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
        $response = curl_exec($ch);
        curl_close($ch);
    }
}

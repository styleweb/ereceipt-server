<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Receipt;
use App\Models\ReceiptTemplate;
use App\Models\Invoice;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return Receipt::with('estimate_logs')->get();
        $word = $request->input('word');
        $query = Receipt::query();
        // 検索
        $query->when($word, function ($query, $word) {
            return $query->where('superscription', "LIKE", "%$word%")->orWhere('id', $word);
        });
        return $query->orderBy('id', 'desc')->with('customer')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        try {
            $result = Receipt::updateOrCreate(
                ['id' => $request->id],
                $request->toArray()
            );
            if ($request->save_template) {
                ReceiptTemplate::create(
                    $request->toArray()
                );
            }

            return $result;
        } catch (\Exception $e) {
            report($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Receipt::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Receipt::find($id);
        $item->name = $request->name;
        $item->save();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Receipt::find($id);
        return $item->delete();
    }


    public function pdf($id)
    {
        $item = Receipt::find($id);

        $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

        $title = $item->number . '_御見積書.pdf';

        $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
        $pdf->setPaper('A4');
        return $pdf->stream($title);
    }


    public function public($uuid)
    {
        $item = Receipt::where('uuid', $uuid)->first();

        if ($item) {
            $session_key = "estimate_{$item->id}";
            // sessionが存在しないなら追加
            if (session()->exists($session_key)) {
                // 前回アクセスが、30分より前なら追加
                $access = session()->get($session_key);
                if ($access < strtotime("-5 min")) {
                    $this->addLog($item);
                }
            } else {
                $this->addLog($item);
            }

            $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

            $title = $item->number . '_御見積書.pdf';

            $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
            $pdf->setPaper('A4');
            return $pdf->stream($title);
        } else {
            return abort(404);
        }
    }

    protected function addLog($item)
    {
        if (!Auth::check()) {
            $session_key = "estimate_{$item->id}";
            $logs = $item->logs ?? [];
            $data = [
                'date' => date('Y/n/j H:i:s'),
                'ip' => $_SERVER["REMOTE_ADDR"],
                'user_agent' => $_SERVER["HTTP_USER_AGENT"]
            ];
            // Log::debug($logs);
            array_unshift($logs, $data);
            $item->logs = $logs;
            $item->save();
            session()->put($session_key, time());
        }
    }

    public function csv()
    {
        // コールバック関数に１行ずつ書き込んでいく処理を記述
        $callback = function () {
            // 出力バッファをopen
            $stream = fopen('php://output', 'w');
            // 文字コードをShift-JISに変換
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');
            // ヘッダー行
            fputcsv($stream, [
                'ID',
            ]);
            // データ
            $items = Receipt::orderBy('id', 'desc');
            // ２行目以降の出力
            // cursor()メソッドで１レコードずつストリームに流す処理を実現できる。
            foreach ($items->cursor() as $item) {
                fputcsv($stream, [
                    $item->id,
                ]);
            }
            fclose($stream);
        };

        // 保存するファイル名
        $filename = sprintf('お見積り-%s.csv', date('Ymd'));

        // ファイルダウンロードさせるために、ヘッダー出力を調整
        $header = [
            'Content-Type' => 'application/octet-stream',
        ];

        return response()->streamDownload($callback, $filename, $header);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function design($design)
    {
        $item = Receipt::find(1);

        $pdf = PDF::loadView("pdf/estimate/{$design}", ['item' => $item]);
        $pdf->setPaper('A4');
        return $pdf->stream();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function from_invoice($id)
    {
        $defaultTemplateId = Auth::user()->group->receipt_template_id;
        $item = ReceiptTemplate::where('group_id', Auth::user()->group_id)->where('id', $defaultTemplateId)->first();
        $item->setHidden(['id']);

        $fromItem = Invoice::where('group_id', Auth::user()->group_id)->where('id', $id)->first();

        $item->superscription  = $fromItem->superscription;
        $item->items = $fromItem->items;
        $item->sub_total = $fromItem->sub_total;
        $item->total = $fromItem->total;
        $item->design = $fromItem->design;

        // if ($item) {
        //     unset($item->id);
        //     unset($item->title);
        //     unset($item->text);
        //     unset($item->number);
        //     unset($item->issue_date);
        //     unset($item->expire_date);
        // }

        return $item;
    }
}

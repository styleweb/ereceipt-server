<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EstimateTemplate;
use App\Models\Group;
use Illuminate\Support\Facades\Auth;

class EstimateTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = EstimateTemplate::query();
        $query->where('group_id', Auth::user()->group_id);

        // 検索
        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            return $query->where('superscription', "LIKE", "%$word%")->orWhere('id', $word);
        });
        return $query->orderBy('id', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return EstimateTemplate::updateOrCreate(
            [
                'id' => $request->id,
                'group_id' => Auth::user()->group_id,
                'user_id' => Auth::user()->id
            ],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        return EstimateTemplate::where('group_id', Auth::user()->group_id)->where('id', $id)->first();
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = EstimateTemplate::where('group_id', Auth::user()->group_id)->find($id);
        return $item->delete();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_group()
    {
        $item = Group::where('id', Auth::user()->group_id)->first();

        if ($item) {
            unset($item->id);
            unset($item->name);
        }

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_copy($id = null)
    {
        $item =
            EstimateTemplate::with('group')->where('group_id', Auth::user()->group_id)->where('id', $id)->first();

        if ($item) {
            unset($item->id);
            $item->information = $item->group->information;
            $item->stamp_name = $item->group->stamp_name;
        }
        return $item;
    }
}

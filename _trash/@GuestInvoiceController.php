<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GuestInvoice;
use Barryvdh\DomPDF\Facade\Pdf;


class GuestInvoiceController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return GuestInvoice::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return GuestInvoice::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = GuestInvoice::find($id);
        return $item->delete();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdf($id)
    {
        $item = GuestInvoice::find($id);

        $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

        $title = $item->number . '_請求書.pdf';

        $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
        $pdf->setPaper('A4');
        return $pdf->stream($title);
    }


    public function public($uuid)
    {
        $item = GuestInvoice::where('uuid', $uuid)->first();

        if ($item) {

            $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

            $title = $item->number . '_御請求書.pdf';

            $pdf = PDF::loadView("pdf/invoice/{$design}", ['item' => $item, 'title' => $title]);
            $pdf->setPaper('A4');
            return $pdf->stream($title);
        } else {
            return abort(404);
        }
    }
}

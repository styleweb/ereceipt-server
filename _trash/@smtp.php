<?php

return [
    'invoice' => [
        'mailer' => env('INVOICE_MAIL_MAILER', 'smtp'),
        'host' => env('INVOICE_MAIL_HOST', 'styleweb'),
        'port' => env('INVOICE_MAIL_PORT', '587'),
        'username' => env('INVOICE_MAIL_USERNAME', 'styleweb'),
        'password' => env('INVOICE_MAIL_PASSWORD', ''),
        'encryption' => env('INVOICE_MAIL_ENCRYPTION', 'ssl'),
        'from_address' => env('INVOICE_MAIL_FROM_ADDRESS', ''),
        'from_name' => env('INVOICE_MAIL_FROM_NAME', ''),
    ],
    'estimate' => [
        'mailer' => env('ESTIMATE_MAIL_MAILER', 'smtp'),
        'host' => env('ESTIMATE_MAIL_HOST', 'styleweb'),
        'port' => env('ESTIMATE_MAIL_PORT', '587'),
        'username' => env('ESTIMATE_MAIL_USERNAME', 'styleweb'),
        'password' => env('ESTIMATE_MAIL_PASSWORD', ''),
        'encryption' => env('ESTIMATE_MAIL_ENCRYPTION', 'ssl'),
        'from_address' => env('ESTIMATE_MAIL_FROM_ADDRESS', ''),
        'from_name' => env('ESTIMATE_MAIL_FROM_NAME', ''),
    ],
];

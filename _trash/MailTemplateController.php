<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MailTemplate;
use Illuminate\Support\Facades\Auth;

class MailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $word = $request->input('word');
        $query = MailTemplate::query();
        // 検索
        $query->when($word, function ($query, $word) {
            return $query->where('superscription', "LIKE", "%$word%")->orWhere('id', $word);
        });
        return $query->orderBy('created_at', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        return MailTemplate::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 権限がない場合アクセス不可の処理を入れる
        $groupId = Auth::user()->group_id;
        return MailTemplate::where('type', $id)->where('group_id', $groupId)->first();
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function format($type = null)
    // {
    //     $replacement = [
    //         '{顧客名}' => 'こんばんは',
    //         '%%word2%%' => '綺麗'
    //     ];

    //     $template =
    //         MailTemplate::where('group_id', Auth::user()->group_id)->where('type', $type)->first();

    //     $template->body = str_replace(array_keys($replacement), array_values($replacement), $template->body);


    //     return $template;
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupId = Auth::user()->group_id;
        $item = MailTemplate::find($groupId);

        return $item->update(
            $request->toArray()
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = MailTemplate::find($id);
        return $item->delete();
    }
}

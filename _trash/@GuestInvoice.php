<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestInvoice extends Model
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'remarks' => 'json'];

    protected $fillable = [
        'title',
        'text',
        'superscription',
        'attention',
        'number',
        'issue_date',
        'issue_format',
        'expire_date',
        'expire_format',
        'details',
        'items',
        'sub_total',
        'tax',
        'total',
        'information',
        'stamp_type',
        'stamp_shape',
        'stamp_name',
        'stamp_image',
        'company_image',
        'remarks',
        'enabled',
        'design',
    ];

    /**
     * @return void
     */
    protected static function booted()
    {
        // データ作成時
        static::creating(function (GuestInvoice $invoice) {

            //公開用ハッシュを作成する
            $uuid = str_split(mt_rand(0, 99999999) . sha1(date('YmdHis')), 40);
            $invoice->uuid = $uuid[0];
            return $invoice;
        });
    }
}

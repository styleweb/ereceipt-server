<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GuestEstimate;
use Barryvdh\DomPDF\Facade\Pdf;

class GuestEstimateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return GuestEstimate::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview($uuid)
    {
        $item = GuestEstimate::where('uuid', $uuid)->first();

        if ($item) {
            $pdf = PDF::loadView('estimate_nomal', ['item' => $item]);
            $pdf->setPaper('A4');

            $item->delete(); // アクセスで削除
            return $pdf->stream();
        } else {
            return abort(404);
        }
    }


    public function public($uuid)
    {
        $item = GuestEstimate::where('uuid', $uuid)->first();

        if ($item) {

            $pdf = PDF::loadView('estimate_nomal', ['item' => $item]);
            $pdf->setPaper('A4');
            return $pdf->stream();
        } else {
            return abort(404);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MailSetting;
use Illuminate\Support\Facades\Auth;

class MailSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupId = Auth::user()->group_id;
        return MailSetting::where('group_id', $groupId)->first();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        return MailSetting::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 権限がない場合アクセス不可の処理を入れる
        $groupId = Auth::user()->group_id;
        return MailSetting::where('type', $id)->where('group_id', $groupId)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupId = Auth::user()->group_id;
        $item = MailSetting::find($groupId);

        return $item->update(
            $request->toArray()
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = MailSetting::find($id);
        return $item->delete();
    }
}

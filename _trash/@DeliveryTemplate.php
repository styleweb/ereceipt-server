<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class DeliveryTemplate extends Model
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'remarks' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'title',
        'text',
        'superscription',
        'attention',
        'amount',
        // 'number',
        'number_format',
        // 'issue_date',
        'issue_format',
        // 'expire_date',
        'expire_type',
        'expire_calc_method',
        'expire_format',
        'expire_days',
        'expire_text',
        'details',
        'subject',
        'items',
        'sub_total',
        'tax',
        'total',
        'information',
        'stamp_type',
        'stamp_shape',
        'stamp_name',
        'stamp_image',
        'company_image',
        'remarks',
        'enabled',
        'design',

        // ここから下はテンプレートのみ
        'name',

    ];

    /**
     * @return void
     */
    protected static function booted()
    {

        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });
    }
}

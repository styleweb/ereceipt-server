<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estimate;
use App\Models\EstimateTemplate;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class EstimateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $word = $request->input('word');
        $customer_id = $request->input('customer_id');
        $state = $request->input('state');
        $query = Estimate::query();
        $query->when($state, function ($query, $state) {
            return $query->where('state', $state);
        });
        $query->when($word, function ($query, $word) {
            return $query->where('superscription', "LIKE", "%$word%")->orWhere('number', "LIKE", "%$word%")->orWhere('details', "LIKE", "%$word%");
        });
        $query->when($customer_id, function ($query, $customer_id) {
            return $query->where('customer_id', $customer_id);
        });
        return $query->orderBy('id', 'desc')->with('customer')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->save_template) {
            EstimateTemplate::create(
                $request->toArray()
            );
        }

        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        return Estimate::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        return Estimate::where('uuid', $uuid)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $item = Estimate::find($id);
        // $item->name = $request->name;
        // $item->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $item = Estimate::where('uuid', $uuid)->first();
        return $item->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function copy(Request $request)
    {
        // Model copy.
        $item = Estimate::where('uuid', $request->uuid)->first()->replicate();

        // Remove unnecessary data.
        unset($item->logs);

        // Numbering.
        $template_id = Auth::user()->group->template_default['estimate'];
        $number_template = EstimateTemplate::find($template_id);
        $item->number = $this->replace_number($number_template->number);

        return $item->create($item->toArray());
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdf($uuid)
    {
        $item = Estimate::where('uuid', $uuid)->first();

        $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

        $title = '見積書.pdf';

        // \Log::debug($item->details);

        foreach ($item->details as $row) {
            if ($row['title'] === '件名') {
                $title = $row['content'] . '_' . $title;
            }
        }

        $title = $item->number . '_' . $title;

        $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
        $pdf->setPaper('A4');
        return $pdf->stream($title);
    }


    public function public($uuid)
    {
        // modelにgroup_idの条件がついてるので、publicの場合だけ、条件を外す
        $item = Estimate::withoutGlobalScopes()->where('uuid', $uuid)->first();

        if ($item) {
            $session_key = "invoice_{$item->id}";
            // sessionが存在しないなら追加
            if (session()->exists($session_key)) {
                // 前回アクセスが、30分より前なら追加
                $access = session()->get($session_key);
                if ($access < strtotime("-5 min")) {
                    $this->addLog($item);
                }
            } else {
                $this->addLog($item);
            }

            $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

            $title = $item->number . '_御請求書.pdf';

            $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
            $pdf->setPaper('A4');
            return $pdf->stream($title);
        } else {
            return abort(404);
        }
    }

    protected function addLog($item)
    {
        $session_key = "estimate_{$item->id}";
        $logs = $item->logs ?? [];
        $data = [
            'date' => date('Y/n/j H:i:s'),
            'ip' => $_SERVER["REMOTE_ADDR"],
            'user_agent' => $_SERVER["HTTP_USER_AGENT"]
        ];
        // Log::debug($logs);
        array_unshift($logs, $data);
        $item->logs = $logs;
        $item->save();
        session()->put($session_key, time());
    }

    public function csv()
    {
        // コールバック関数に１行ずつ書き込んでいく処理を記述
        $callback = function () {
            // 出力バッファをopen
            $stream = fopen('php://output', 'w');
            // 文字コードをShift-JISに変換
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');
            // ヘッダー行
            fputcsv($stream, [
                'ID',
            ]);
            // データ
            $items = Estimate::orderBy('id', 'desc');
            // ２行目以降の出力
            // cursor()メソッドで１レコードずつストリームに流す処理を実現できる。
            foreach ($items->cursor() as $item) {
                fputcsv($stream, [
                    $item->id,
                ]);
            }
            fclose($stream);
        };

        // 保存するファイル名
        $filename = sprintf('お見積り-%s.csv', date('Ymd'));

        // ファイルダウンロードさせるために、ヘッダー出力を調整
        $header = [
            'Content-Type' => 'application/octet-stream',
        ];

        return response()->streamDownload($callback, $filename, $header);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_template($id = null)
    {
        if (is_null($id)) {
            $id = Auth::user()->group->template_default->estimate;
        }
        $item = EstimateTemplate::where('id', $id)->first();
        if (!empty($item)) {
            $item->setHidden(['id']);
            return $item;
        }
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_copy($id = null)
    {
        $item =
            Estimate::with('group')->where('group_id', Auth::user()->group_id)->where('id', $id)->first();
        $item->setHidden(['id']);

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function from_estimate($id)
    {
        $defaultTemplateId = Auth::user()->group->invoice_template_id;
        $item = EstimateTemplate::where('group_id', Auth::user()->group_id)->where('id', $defaultTemplateId)->first();
        $item->setHidden(['id']);

        $estimate =
            Estimate::with('group')->where('group_id', Auth::user()->group_id)->where('id', $id)->first();

        $item->superscription  = $estimate->superscription;
        $item->items = $estimate->items;
        $item->sub_total = $estimate->sub_total;
        $item->total = $estimate->total;
        $item->design = $estimate->design;

        // if ($item) {
        //     unset($item->id);
        //     unset($item->title);
        //     unset($item->text);
        //     unset($item->number);
        //     unset($item->issue_date);
        //     unset($item->expire_date);
        //     unset($item->remarks);
        // }

        return $item;
    }


    public function replace_number($number = '')
    {
        $item = Estimate::createNumberFromFormat(
            Auth::check() ? Auth::user()->group->invoice_numbering : null,
            $number,
        );
        return $item;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Receipt extends Model
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'remarks' => 'json', 'logs' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'title',
        'text',
        'superscription',
        'attention',
        'amount',
        'number',
        'issue_date',
        'issue_format',
        'details',
        'subject',
        'items',
        'sub_total',
        'tax',
        'total',
        'information',
        'stamp_type',
        'stamp_shape',
        'stamp_name',
        'stamp_image',
        'company_image',
        'remarks',
        'enabled',
        'logs',
        'design',
    ];

    /**
     * @return Array
     */
    public function updateNumbering()
    {
        $group = Group::where('id', Auth::user()->group_id)->first();
        $numbering = $group->receipt_numbering;

        $numbering['total'] = $numbering['total'] + 1;
        if ($numbering['year']['date'] == date('Y')) {
            $numbering['year']['count'] = $numbering['year']['count'] + 1;
        } else {
            $numbering['year']['count'] = 1;
            $numbering['year']['date'] = date('Y');
        }
        if ($numbering['month']['date'] == date('m')) {
            $numbering['month']['count'] = $numbering['month']['count'] + 1;
        } else {
            $numbering['month']['count'] = 1;
            $numbering['month']['date'] = date('m');
        }
        if ($numbering['day']['date'] == date('d')) {
            $numbering['day']['count'] = $numbering['day']['count'] + 1;
        } else {
            $numbering['day']['count'] = 1;
            $numbering['day']['date'] = date('d');
        }
        $group->receipt_numbering = $numbering;
        $group->save();

        return $group->receipt_numbering;
    }

    /**
     * @return string
     */
    public function createNumberFromFormat($numbering, $number, $issue_date, $customer_id)
    {
        $number = str_replace('{発行日:ymd}', date('ymd', strtotime($issue_date)), $number);
        $number = str_replace('{発行日:Ymd}', date('Ymd', strtotime($issue_date)), $number);
        $number = str_replace('{連番:月,4}', str_pad($numbering['month']['count'], 4, '0', STR_PAD_LEFT), $number);
        $number = str_replace('{連番:月,3}', str_pad($numbering['month']['count'], 3, '0', STR_PAD_LEFT), $number);
        return $number;
    }

    /**
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (Receipt $receipt) {
            $receipt->uuid = Str::uuid();
            return $receipt;
        });

        static::created(function (Receipt $item) {

            // 自動採番 groupの連番を更新する
            $numbering = $item->updateNumbering();

            // 番号を帳票に保存
            $item->number = $item->createNumberFromFormat(
                $numbering,
                $item->number,
                $item->issue_date,
                $item->customer_id
            );
            $item->save();
        });
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}

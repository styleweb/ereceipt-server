<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestEstimate extends Model
{
    use HasFactory;


    protected $casts = ['items' => 'json', 'remarks' => 'json'];

    protected $fillable = [
        'superscription',
        'attention',
        'amount',
        'number',
        'issue_date',
        'expire_date',
        'items',
        'sub_total',
        'tax',
        'total',
        'information',
        'stamp_type',
        'stamp_shape',
        'stamp_name',
        'stamp_image',
        'company_image',
        'remarks',
        'enabled',
    ];


    /**
     * @return void
     */
    protected static function booted()
    {
        // データ作成時に、公開用ハッシュを作成する
        static::creating(function (GuestEstimate $guest_estimate) {

            $uuid = str_split(mt_rand(0, 99999999) . sha1(date('YmdHis')), 40);
            $guest_estimate->uuid = $uuid[0];
            return $guest_estimate;
        });
    }
}

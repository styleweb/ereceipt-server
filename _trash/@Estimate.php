<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Estimate extends Document
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'stamp' => 'json', 'remarks' => 'json', 'logs' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'title',
        'text',
        'superscription',
        'attention',
        'number',
        'issue_date',
        'issue_format',
        'expire_date',
        'expire_type',
        'expire_calc_method',
        'expire_format',
        'expire_days',
        'expire_text',
        'details',
        'subject',
        'items',
        'sub_total',
        'tax',
        'total',
        'stamp',
        'remarks',
        'enabled',
        'logs',
        'design',
    ];


    /**
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (Estimate $estimate) {

            $estimate->uuid = Str::uuid();
            return $estimate;
        });


        static::created(function (Estimate $estimate) {

            // 自動採番 groupの連番を更新する
            $numbering = $estimate->updateNumbering('estimate');

            // 番号を帳票に保存
            $estimate->number = $estimate->createNumberFromFormat($numbering, $estimate->number, $estimate->issue_date, $estimate->customer_id);
            $estimate->save();
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'tax_rate',
        'stamp_type',
        'information',
        'stamp_shape',
        'stamp_name',
        'stamp_image',
        'company_image',
        'remarks',
    ];

    public function users()
    {
        // return $this->hasMany(User::class);
    }
}

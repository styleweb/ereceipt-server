<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body style="background: #edf2f7">

    <div class="container" style="max-width: 800px;">
        <div class="card">
            <div class="p-5">
                <div class="row">
                    <div class="col-2">1</div>
                    <div class="col-10">
                        <div style="white-space: pre-wrap;">

                            スタイルウェブ <system@mypdf.jp>
                                12:28 (2 時間前)
                                To 自分

                                株式会社 ミルコ 御中

                                いつもお世話になっております。

                                お知らせが一件ございます。
                                下記のURLをクリックしてご確認をお願いします。
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>
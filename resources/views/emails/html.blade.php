<?php //Log::debug($content);
foreach ($content['blocks'] as $key => $value) {
    switch ($value['type']) {
        case 'paragraph':
            echo '<p>' . $value['data']['text'] . '</p>';
            break;

        case 'image':
            echo '<img src="' . $value['data']['file']['url'] . '" alt="' . $value['data']['caption'] . '">';
            break;

        default:
            # code...
            break;
    }
}

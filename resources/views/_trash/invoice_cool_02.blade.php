<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Whisper&display=swap" rel="stylesheet">
    <style type="text/css">
        @font-face {
            font-family: ipag;
            src: url("{{ storage_path('fonts/ipaexg.ttf') }}") format('truetype');
        }

        * {
            margin: 0;
            padding: 0;
        }


        body {
            position: relative;
            font-family: ipag !important;
            font-family: "ヒラギノ明朝 Pro", "Hiragino Mincho Pro", "MS P明朝", "MS PMincho", serif;
            color: #000;
            font-size: 80%;
            z-index: -10;
        }

        body,
        div,
        span,
        dl,
        dt,
        dd,
        ul,
        ol,
        li,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        th,
        td,
        form,
        fieldset,
        input,
        textarea {
            padding: 0;
            margin: 0;
            text-align: left;
            /* line-height: 1.6; */
        }

        h1 {
            font-size: 24px;
            text-align: center;
            margin-bottom: 1.5em;
        }

        h2 {
            font-size: 15px;
        }

        h3 {
            font-size: 40px;
            border-bottom: 1px solid #000;
        }

        h4 {
            font-size: 25px;
        }

        h5 {
            border-bottom: 1px solid #000;
        }

        .small {
            font-size: 10px;
        }


        /* 請求書タイトル囲い */
        .title {
            background: #303741;
            padding: 0 50px 30px;
        }

        /* 英語タイトル */
        .en {
            font-size: 80px;
            font-family: 'Bebas Neue', cursive;
            color: #ffffff;
            text-align: left;
            letter-spacing: 0.1em;
        }

        /* 日本語タイトル */
        .ja {
            display: block;
            color: #ffffff;
            text-align: left;
        }

        /* 宛名・金額などの上部 */
        .invoice_top {
            background: #F2F3F7;
            padding: 50px 50px 20px 50px;
            margin-bottom: 80px;
        }

        /* 宛名 */
        .name {
            width: 40%;
            font-size: 18px;
            margin-bottom: 30px;
        }

        /* 宛名（様） */
        .name span {
            font-size: 14px;
        }

        .flex {
            display: flex;
            justify-content: space-between;
        }

        .info {
            margin-bottom: 20px;
        }

        /* ナンバー */
        .no {
            font-size: 14px;
            font-family: 'Bebas Neue', cursive;
            margin-bottom: 5px;
            text-align: right;
        }

        /* 発行日 */
        .date {
            font-family: 'Bebas Neue', cursive;
            font-size: 14px;
            text-align: right;
        }


        /* 合計金額 */
        .charge {
            width: 50%;
            /* background: #303741;
            color: #ffffff; */
            padding: 5px 15px;
            margin-bottom: 50px;
        }

        .charge .c_top {
            display: block;
            font-size: 12px;
            padding-bottom: 15px;
        }

        .charge .c_b {
            display: block;
            font-size: 24px;
        }

        p {
            padding: 0 50px 30px;
        }

        .tbl {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 50px;
            padding: 0 50px;
        }


        .tbl tr th {
            background: #303741;
            color: #ffffff;
            font-size: 14px;
            padding: 10px 20px 10px;
        }

        .tbl tr td {
            vertical-align: middle;
            padding: 15px;
            border-bottom: dotted 1px #CCCCCC;
        }

        .child_date {
            font-size: 10px;
        }

        .ar {
            text-align: right;
        }

        .al {
            text-align: left;
        }

        .memo {
            width: 33%;
            float: left;
            margin-bottom: 2em;
        }

        .space {
            padding-right: 6%;
        }

        .memo .title {
            font-weight: bold;
        }

    </style>
</head>
<div class="title">
    <div class="en">Invoice</div>
    <div class="ja">請求書</div>
</div>


{{-- 上部 --}}
<div class="invoice_top">
    <div class="name"><?php echo $invoice['Customer']['name'] ?? ''; ?>　　<span>様</span></div>

    <div class="flex">
        <div class="info">
            <div class="no">NO.<?php echo $invoice['Invoice']['id'] ?? ''; ?></div>
            <div class="date">DATE　<?php echo $invoice['Invoice']['issue_date'] ?? ''; ?></div>
        </div>
        <div class="charge"><span class="c_top">TOTAL</span><span class="c_b">￥</span></div>
    </div>
</div>



{{-- 下部 --}}
<p>下記のとおりご請求いたします。</p>
<table class="tbl">
    <tr>
        <th class="al">TITLE</th>
        <th class="al">DESCRIPTION</th>
        <th class="ar">SUBTOTAL</th>
    </tr>
    <tr>
        <td>
            <div class="f14"></div>
        </td>
        <td></td>
        <td class="ar">￥</td>
    </tr>
    <tr>
        <td class="ar" colspan="2">小計</td>
        <td class="ar">￥</td>
    </tr>
    <tr>
        <td class="ar" colspan="2">消費税</td>
        <td class="ar">￥</td>
    </tr>
</table>


<div class="memo">
    <div class="memo_title"><?php echo $invoice['Invoice']['remarks_title'] ?? ''; ?></div>
    <div class="content">
        <?php echo $invoice['Invoice']['remarks'] ?? ''; ?>
    </div>
</div>

</body>

</html>

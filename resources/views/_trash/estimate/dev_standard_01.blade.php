<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{public_path('css/pdf/default.css')}}">
    <style type="text/css">
        .main_column {
            padding: 70px;
        }

        body {
            font-family: mp;
            color: #333;
        }

        .en {
            font-family: "sans-serif";
        }
    </style>
</head>
<div class="main_column">
    <div class="header ac">
        <div style="font-size: 24px; margin-bottom: 30px">御見積書</div>
    </div>

    <table style="margin-bottom: 20px">
        <tr>
            <td>
                @component('components.customer')
                @slot('item', $item)
                @endcomponent
            </td>
            <td>
                @component('components.date')
                @slot('item', $item)
                @endcomponent
            </td>
        </tr>
    </table>
    <div class="cf" style="margin-bottom: 20px">
        <div style="float:left; width: 50%">

            <div style="margin-bottom: 20px">
                @component('components.details')
                @slot('item', $item)
                @endcomponent

                @component('components.expire')
                @slot('item', $item)
                @endcomponent
            </div>
            <div>
                @component('components.total')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
        <div style="float:right;">
            <div>
                @component('components.stamp')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
    </div>
    <div style="margin-bottom: 20px;">
        @component('components.page2description')
        @slot('item', $item)
        @endcomponent
    </div>
    <div>
        @component('components.remarks')
        @slot('item', $item)
        @endcomponent
    </div>
</div>


<div style="page-break-after: always;"></div>
<div class="main_column">
    <table style="margin-bottom: 20px">
        <tr>
            <td></td>
            <td style="font-size:18px;">御見積明細　Page 2</td>
            <td>
                @component('components.date')
                @slot('item', $item)
                @endcomponent
            </td>
        </tr>
    </table>

    @component('components.items')
    @slot('item', $item)
    @endcomponent
</div>

<div style="page-break-after: always;"></div>
<div class="main_column">
    <table style="margin-bottom: 20px">
        <tr>
            <td></td>
            <td style="font-size:18px;">御見積明細　Page 3</td>
            <td>
                @component('components.date')
                @slot('item', $item)
                @endcomponent
            </td>
        </tr>
    </table>

    @component('components.items')
    @slot('item', $item)
    @endcomponent

    <div>
        @component('components.items_total')
        @slot('item', $item)
        @endcomponent
    </div>
</div>
</body>

</html>
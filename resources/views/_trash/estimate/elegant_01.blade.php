<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{public_path('css/pdf/default.css')}}">

    <style type="text/css">
        body {
            font-family: "shippori";
            font-size: 14px;
            background: url("{{ storage_path('app/public/elegant_pink/elegant_pink_bg.png') }}");
            background-repeat: repeat-y;
            z-index: -10;
        }

        body::before {
            position: absolute;
            content: url("{{ storage_path('app/public/elegant_pink/elegant_pink_bg_02.png') }}");
            top: -40px;
            right: -10px;
            z-index: -20;
        }

        body::after {
            position: absolute;
            content: url("{{ storage_path('app/public/elegant_pink/elegant_pink_bg_03.png') }}");
            bottom: -30px;
            left: -30px;
            z-index: -20;
        }

        #content {
            padding: 0 70px 0 100px;
        }

        /* 請求書タイトル囲い */
        .header {
            text-align: center;
            margin-bottom: 40px;
            color: #fcaba7;
        }

        .en {
            font-family: serif;
        }
    </style>
</head>
<div id="content">
    <div class="header">
        <div style="font-size: 80px">見積書</div>
    </div>
    <div class="cf" style="margin-bottom: 20px">
        <div style="float:left; width: 50%">
            <div style="margin-bottom: 20px">
                @component('components.customer')
                @slot('item', $item)
                @endcomponent
            </div>

            <div>
                @component('components.total')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
        <div style="float:right;">
            <div style="margin-bottom: 30px">
                @component('components.date')
                @slot('item', $item)
                @endcomponent
            </div>
            <div>
                @component('components.stamp')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
    </div>
    <div style="margin-bottom: 20px;">
        @component('components.items')
        @slot('item', $item)
        @endcomponent
    </div>
    <div>
        @component('components.remarks')
        @slot('item', $item)
        @endcomponent
    </div>
</div>

</body>

</html>
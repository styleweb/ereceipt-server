<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Satisfy&display=swap" rel="stylesheet">
    <style type="text/css">
        @font-face {
            font-family: ipag;
            src: url("{{ storage_path('fonts/ipaexg.ttf') }}") format('truetype');
        }

        * {
            margin: 0;
            padding: 0;
        }


        body {
            position: relative;
            font-family: ipag !important;
            font-family: "ヒラギノ明朝 Pro", "Hiragino Mincho Pro", "MS P明朝", "MS PMincho", serif;
            background: #fff;
            color: #333333;
            font-size: 80%;

            z-index: -10;

        }

        body::before {
            position: absolute;
            content: url('{{ storage_path('app/public/cute/cute_bg_top.png') }}');
            top: 0;
            right: 0;
            left: 0;
            margin: 0 auto;
            width: 100%;
            z-index: -20;
        }

        body::after {
            position: absolute;
            content: url('{{ storage_path('app/public/cute/cute_bg_bottom.png') }}');
            bottom: 0;
            right: 0;
            left: 0;
            margin: 0 auto;
            width: 100%;
            z-index: -20;
        }

        body,
        div,
        span,
        dl,
        dt,
        dd,
        ul,
        ol,
        li,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        th,
        td,
        form,
        fieldset,
        input,
        textarea {
            padding: 0;
            margin: 0;
            text-align: left;
            line-height: 1.6;
        }

        h1 {
            font-size: 24px;
            text-align: center;
            margin-bottom: 1.5em;
        }

        h2 {
            font-size: 15px;
        }

        h3 {
            font-size: 40px;
            border-bottom: 1px solid #000;
        }

        h4 {
            font-size: 25px;
        }

        h5 {
            border-bottom: 1px solid #000;
        }

        .small {
            font-size: 10px;
        }


        /* 請求書タイトル囲い */
        .title {
            text-align: center;
            padding-top: 30px;
            margin-bottom: 20px;
        }

        /* 英語タイトル */
        .en {
            font-size: 60px;
            font-family: 'Satisfy', cursive;
            text-align: center;
            color: #A2D7DB;
        }

        /* 日本語タイトル */
        .ja {
            display: block;
            text-align: center;
            color: #A2D7DB;
        }

        /* 宛名・金額などの上部 */
        .invoice_top {
            padding: 0 50px 20px 50px;
            margin-bottom: 5px;
        }

        /* 宛名 */
        .name {
            width: 40%;
            font-size: 18px;
            border-bottom: solid 1px #CCCCCC;
            padding-bottom: 5px;
        }

        /* 宛名（様） */
        .name span {
            font-size: 14px;
        }



        .flex {
            display: flex;
            justify-content: space-between;
        }

        /* ナンバー */
        .no {
            font-size: 16px;
            color: #A2D7DB;
            font-family: 'Satisfy', cursive;
            text-align: right;
        }

        /* 発行日 */
        .date {
            font-size: 16px;
            color: #A2D7DB;
            font-family: 'Satisfy', cursive;
            text-align: right;
        }

        /* 合計金額 */
        .charge {
            width: 50%;
            padding: 0 15px;
        }

        .charge .c_top {
            display: block;
            font-size: 18px;
            font-family: 'Satisfy', cursive;
            padding-bottom: 10px;
        }

        .charge .c_b {
            display: block;
            font-size: 24px;
        }

        p {
            padding: 0 50px 30px;
        }


        .tbl {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 50px;
            padding: 0 50px;
        }


        .tbl tr th {
            font-size: 14px;
            border-bottom: solid 2px #CCCCCC;
            padding: 5px 20px 5px;
        }

        .tbl tr td {
            vertical-align: middle;
            padding: 15px;
            border-bottom: solid 1px #CCCCCC;
        }

        .child_date {
            font-size: 10px;
        }

        .ar {
            text-align: right;
        }

        .al {
            text-align: left;
        }

        .memo {
            width: 33%;
            float: left;
            margin-bottom: 2em;
        }

        .space {
            padding-right: 6%;
        }

        .memo .title {
            font-weight: bold;
        }
    </style>
</head>
<div class="title">
    <div class="en">Invoice</div>
    <div class="ja">請求書</div>
</div>


{{-- 上部 --}}
<div class="invoice_top">
    <div class="name"><?php echo $invoice['Customer']['name'] ?? ''; ?>　　<span>様</span></div>

    <div class="flex">
        <div class="info">
            <div class="no">no.<?php echo $invoice['Invoice']['id'] ?? ''; ?></div>
            <div class="date">date　<?php echo $invoice['Invoice']['issue_date'] ?? ''; ?></div>
        </div>
        <div class="charge"><span class="c_top">total</span><span class="c_b">￥</span>
        </div>
    </div>
</div>


{{-- 下部 --}}
<p>下記のとおりご請求いたします。</p>
<table class="tbl">
    <tr>
        <th class="al">title</th>
        <th class="al">description</th>
        <th class="ar">subtotal</th>
    </tr>
    <tr>
        <td>
            <div class="f14"></div>
        </td>
        <td></td>
        <td class="ar">￥</td>
    </tr>
    <tr>
        <td class="ar" colspan="2">小計</td>
        <td class="ar">￥</td>
    </tr>
    <tr>
        <td class="ar" colspan="2">消費税</td>
        <td class="ar">￥</td>
    </tr>
</table>


<div class="memo">
    <div class="memo_title"><?php echo $invoice['Invoice']['remarks_title'] ?? ''; ?></div>
    <div class="content">
        <?php echo $invoice['Invoice']['remarks'] ?? ''; ?>
    </div>
</div>

</body>


</html>
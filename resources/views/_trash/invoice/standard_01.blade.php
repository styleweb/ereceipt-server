<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{public_path('css/pdf/default.css')}}">
    <title><?php echo $title ?></title>
    <style type="text/css">
        #content {
            padding: 0 70px;
        }

        body {
            font-family: mp;
            color: #333;
        }

        .en {
            font-family: "sans-serif";
        }
    </style>
</head>
<div id="content">
    <div class="header ac">
        <div style="font-size: 24px; padding:50px 0 30px">請求書</div>
    </div>

    <table style="margin-bottom: 20px">
        <tr>
            <td>
                @component('components.customer')
                @slot('item', $item)
                @endcomponent
            </td>
            <td>
                @component('components.date')
                @slot('item', $item)
                @endcomponent
            </td>
        </tr>
    </table>
    <div class="cf" style="margin-bottom: 20px">
        <div style="float:left; width: 50%">

            <div style="margin-bottom: 20px">
                @component('components.details')
                @slot('item', $item)
                @endcomponent

                @component('components.expire')
                @slot('title', '支払い期限')
                @slot('item', $item)
                @endcomponent
            </div>
            <div>
                <p style="margin-bottom: 15px;">下記のとおりご請求申し上げます。</p>
                @component('components.total')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
        <div style="float:right;">

            <div>
                @component('components.stamp')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
    </div>
    <div style="margin-bottom: 20px;">
        @component('components.items')
        @slot('item', $item)
        @endcomponent

        @component('components.items_total')
        @slot('item', $item)
        @endcomponent
    </div>
    <div>
        @component('components.remarks')
        @slot('item', $item)
        @endcomponent
    </div>
</div>


</body>


</html>
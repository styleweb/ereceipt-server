<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        @font-face {
            font-family: ipag;
            src: url("{{ storage_path('fonts/ipaexg.ttf') }}") format('truetype');
        }

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            font-family: ipag !important;
            background: #fff;
            color: #000;
            font-size: 80%;
            width: 100%;
        }


        table {
            border-collapse: collapse;
        }

        .cf {
            width: 100%;
        }

        .cf:after {
            content: "";
            display: block;
            clear: both;
        }

        .f8 {
            font-size: 8px;
        }

        .f10 {
            font-size: 10px;
        }

        .f12 {
            font-size: 12px;
        }

        .f14 {
            font-size: 14px;
        }

        .f16 {
            font-size: 16px;
        }

        .f20 {
            font-size: 20px;
        }

        .w_120 {
            width: 120px;
        }

        .w_100p {
            width: 100%;
        }

        .al {
            text-align: left;
        }

        .ar {
            text-align: right;
        }

        .ac {
            text-align: center;
        }

        .f_l {
            float: left;
        }

        .f_r {
            float: right;
        }

        .cb {
            clear: both;
        }

        .relative {
            position: relative;
        }



        #r_no {
            width: 120px;
            text-align: left;
            border-bottom: 1px solid #333;
        }

        #date {
            width: 120px;
            text-align: right;
        }

        #title {
            width: 150px;
            text-align: center;
            font-size: 25px;
            letter-spacing: 10px;
            margin: 0 auto;
        }

        #superscription_box {
            margin-top: 20px;
            margin-bottom: 20px;
            width: 350px;
            border-bottom: 1px solid #333;
        }

        #superscription {
            display: inline-block;
            width: 250px;
            padding-bottom: 5px;
            white-space: nowrap;

        }

        #superscription_after {
            display: inline-block;
            width: 30px;
            padding-bottom: 5px;
            text-align: right;
        }

        #amount_box {
            margin-top: 10px;
        }

        #amount {
            background-color: #eee;
            width: 400px;
            margin: 0 auto;
            padding: 5px 50px;
            font-size: 20px;
            border: 1px solid #333333;
        }

        #receipt_bottom {
            margin-top: 30px;
        }

        #proviso {
            width: 360px;
            margin: 10px auto;
            font-size: 13px;
            border-bottom: 1px solid #333;
            padding: 0;
            background-color: #fff;
            line-height: 14px;
        }

        #proviso_pre {
            width: 40px;
            text-align: left;
            margin-bottom: 5px;
        }

        #proviso_mid {
            width: 250px;
            text-align: center;
            margin-bottom: 5px;
        }

        #proviso_post {
            text-align: right;
            width: 60px;
            margin-bottom: 5px;
        }

        #accepted {
            width: 400px;
            margin: 5px auto;
            padding: 0 50px;
            text-indent: 30px;
            font-size: 13px;
        }



        #revenue_stamp_box {
            width: 55px;
            height: 65px;
            padding: 5px;
            border: 1px dotted #333333;
            color: #777;
            font-size: 10px;
            margin-top: 10px;
        }

        #tax_box {

            padding-left: 20px;
            padding-right: 50px;
        }

        #tax_box td {
            width: 80px;
            font-size: 10px;
            border-bottom: 1px solid #999999;
            padding: 6px 3px 2px 3px;
        }

        .tax_detail {
            text-align: right;
        }

        #company {
            width: 250px;
            text-align: left;
            margin-right: 20px;
        }

        #stamp {
            width: 100px;
            margin-bottom: 15px;
            margin-top: 15px;
            text-align: right;
        }


        .remarks {
            padding-top: 1em;
            font-size: 12px;
        }

        .cm {
            position: absolute;
            bottom: 1em;
            right: 1em;
        }
    </style>
</head>

<body>
    <div id="paper" style="padding:50px">
        <div class="cf">
            <div style="width: 120px; padding-left:100px" class="f_r">
                <div id="r_no">No. </div>
                <div id="date"><?= date('Y年n月j日') ?></div>
            </div>
        </div>
        <div id="title">領収書</div>
        <div id="superscription_box">
            <?= $item['superscription'] ?? '' ?><?= $item['attention'] ?? '' ?>
        </div>


        <div id="amount_box" class="cf">
            <p id="amount">￥<?= $item['amount'] ?? '' ?></p>
        </div>
        <div id="proviso" class="cf">
            <p id="proviso_pre" class="f_l">但し <?= $item['proviso'] ?? '' ?></p>

            <p id="proviso_mid" class="f_l"></p>

            <p id="proviso_post" class="f_r">として</p>
        </div>
        <p id="accepted">上記正に領収致しました。</p>

        <div id="receipt_bottom" class="cf">
            <div id="revenue_stamp_box" class="f_l">電子領収書<br>につき印紙<br>不要</div>
            <div id="tax_box" class="f_l">
                <table>
                    <tr>
                        <td colspan="2">内　訳</td>
                    </tr>
                    <tr>
                        <td>税抜金額</td>
                        <td class="tax_detail"> <?= $item['ex_tax_amount'] ?? '' ?></td>
                    </tr>
                    <tr>
                        <td>消費税額</td>
                        <td class="tax_detail"><?= $item['tax_amount'] ?? '' ?></td>
                    </tr>
                    <tr>
                        <td>消費税率</td>
                        <td class="tax_detail"><?= $item['tax_rate'] ?? '' ?>%</td>
                    </tr>
                </table>
            </div>

            <?php if ($item['stamp_type'] == 1) : ?>

                <div id="company" class="f_l" style="white-space: pre-wrap;"><?= $item['information'] ?? '' ?></div>
                <div id="stamp" class="f_l">
                    <img style="width: 80px;" src="<?= "https://ereceipt-server.stylesv.net/stamp/{$item['stamp_shape']}/{$item['stamp_name']}" ?>" alt="">
                </div>
            <?php else : ?>
                <div id="company" class="f_l">
                    <img style="width: 250px;" src="{{ storage_path('app/public/') }}<?= $item['stamp_image'] ?? '' ?>" alt="">
                </div>
            <?php endif ?>
        </div>

        <div class="remarks">
            <?= $item['remarks'] ?? '' ?>
        </div>
    </div>

    <div class="cm">この領収書は 領収書発行サービス「<a href="https://www.ereceipt.jp/" target="_blank">イーレシート</a>」で作成されました</div>
</body>

</html>
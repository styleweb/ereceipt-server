<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        @font-face {
            font-family: ipag;
            src: url("{{ storage_path('fonts/ipaexg.ttf') }}") format('truetype');
        }

        * {
            margin: 0;
            padding: 0;
        }

        #paper {
            padding: 2em;
        }


        body {
            font-family: ipag !important;
            background: #fff;
            color: #000;
            font-size: 80%;
        }

        body,
        div,
        span,
        dl,
        dt,
        dd,
        ul,
        ol,
        li,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        th,
        td,
        form,
        fieldset,
        input,
        textarea {
            padding: 0;
            margin: 0;
            text-align: left;
            line-height: 1.6;
        }

        h1 {
            font-size: 24px;
            text-align: center;
            margin-bottom: 1.5em;
        }

        h2 {
            font-size: 15px;
        }

        h3 {
            font-size: 40px;
            border-bottom: 1px solid #000;
        }

        h4 {
            font-size: 25px;
        }

        h5 {
            border-bottom: 1px solid #000;
        }

        .small {
            font-size: 10px;
        }

        #name {
            font-size: 18px;
        }

        #name span {
            font-size: 14px;
        }

        #no {
            text-align: right;
            font-size: 12px;
            margin-bottom: 5px;
        }

        #date {
            text-align: right;
            font-size: 12px;
        }

        #charge {
            font-size: 22px;
            margin-bottom: 5px;
            border-bottom: solid 1px #CCCCCC;
            width: 40%;
            text-align: center;
        }

        #charge span {
            font-size: 12px;
        }

        #box {
            width: 100%;
            margin-bottom: 30px;

        }

        #box tr th {
            vertical-align: bottom;
            font-size: 16px;
        }

        #box tr td {
            text-align: right;
            height: 70px;
        }

        #tbl {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 50px;
        }

        #tbl tr {}

        #tbl tr th {
            background: #333;
            color: #FFF;
            font-size: 14px;
            padding: 3px 15px;
        }

        #tbl tr td {
            vertical-align: middle;
            padding: 15px;
            border-bottom: dotted 1px #CCCCCC;
        }

        .child_date {
            font-size: 10px;
        }

        .ar {
            text-align: right;
        }

        .al {
            text-align: left;
        }

        .fb {
            font-weight: bold;
        }

        .f14 {
            font-size: 14px;
        }

        .f16 {
            font-size: 16px;
        }

        .mb05 {
            margin-bottom: 5px;
        }

        .mb10 {
            margin-bottom: 10px;
        }

        .mb15 {
            margin-bottom: 15px;
        }

        .mb20 {
            margin-bottom: 20px;
        }

        #tbl2 {
            margin-bottom: 15px;
        }

        #tbl2 tr th {
            width: 130px;
            text-align: left;
        }

        #tbl2 tr td {
            font-size: 12px;
        }

        .memo {
            width: 33%;
            float: left;
            margin-bottom: 2em;
        }

        .space {
            padding-right: 6%;
        }

        .memo .title {
            font-weight: bold;
        }
    </style>
</head>
<div id="paper">


    <h1>ご請求書</h1>
    <div id="name"><?php echo $invoice['Customer']['name'] ?? '' ?>　　<span>様</span></div>
    <div id="no">NO.<?php echo $invoice['Invoice']['id'] ?? '' ?></div>
    <div id="date">発行日　<?php echo $invoice['Invoice']['issue_date'] ?? '' ?></div>
    <div id="charge">￥</div>

    <table id="box">
        <tr>
            <th>下記のとおりご請求いたします。</th>
            <td>

            </td>
        </tr>
    </table>

    <table id="tbl">
        <tr>
            <th class="al">Title</th>
            <th class="al">Description</th>
            <th class="ar">Subtotal</th>
        </tr>

        <tr>
            <td>
                <div class="f14"></div>
            </td>
            <td></td>
            <td class="ar">￥</td>
        </tr>
        <tr>
            <td class="ar" colspan="2">小計</td>
            <td class="ar">￥</td>
        </tr>
        <tr>
            <td class="ar" colspan="2">消費税</td>
            <td class="ar">￥</td>
        </tr>
    </table>
    <img src="{{ storage_path('app/public/bear.jpg') }}" alt="">



    <div class="memo">
        <div class="title"><?php echo $invoice['Invoice']['remarks_title'] ?? '' ?></div>
        <div class="content">
            <?php echo $invoice['Invoice']['remarks'] ?? '' ?>
        </div>

    </div>

</div>

</body>


</html>
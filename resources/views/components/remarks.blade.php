<div class="remarks cf">
    <?php
    $width = count($item['remarks']) > 1 ? '300px' : '100%';
    $float = 'left';
    foreach ($item['remarks'] as $row) :
        if (!empty($row['title']) || !empty($row['content'])) :
    ?>
            <div class="remark" style="width: <?= $width ?>; float:<?= $float ?>">
                <div style="color:#666; margin-bottom:10px; border-bottom:1px solid #ccc"><?php echo $row['title'] ?? ''; ?></div>
                <div class="content" style="font-size:12px; white-space: pre-wrap;"><?php echo $row['content'] ?? ''; ?></div>
            </div>
    <?php
            $float = 'right';
        endif;
    endforeach ?>
</div>
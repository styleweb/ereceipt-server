<?php if (!empty($item['expire_date'])) : ?>
    <table>
        <tr>
            <td style="width: 70px; font-size:12px; color:#666"> <?= $title ?? '有効期限' ?></td>
            <td><?php echo date('Y年n月j日', strtotime($item['expire_date'])); ?></td>
        </tr>
    </table>
<?php endif ?>
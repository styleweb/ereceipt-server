<div style="border-bottom: solid 3px #eee">
    <table>
        <tr>
            <td class="f-en" style="width: 60px; font-size:12px; color:#666">TOTAL</td>
            <td class="f-en en" style=" font-size: 30px; text-align:center"><span style="font-size: 24px; margin-right:7px;">&yen;</span><?php echo number_format($item['total']) ?? ''; ?></td>
        </tr>
    </table>
</div>
<div>
    <?php
    $width = count($item['details']) > 1 ? '300px' : '100%';
    foreach ($item['details'] as $row) :
        if (!empty($row['title']) || !empty($row['content'])) :
    ?>
            <table>
                <tr>
                    <td style="width: 70px; font-size:12px; color:#666"><?= $row['title'] ?></td>
                    <td style="line-height:1"><?php echo $row['content'] ?? ''; ?></td>
                </tr>
            </table>
    <?php
        endif;
    endforeach ?>
</div>
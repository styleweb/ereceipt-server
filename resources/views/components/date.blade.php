<div class="small cf">
    <table style="float:right">
        <?php if (!empty($item['number'])) : ?>
            <tr>
                <td style=" text-align: right; color:#666">NO.</td>
                <td style="padding-left:2em; text-align: right"><?php echo $item['number'] ?? ''; ?></td>
            </tr>
        <?php endif ?>
        <?php if (!empty($item['issue_date'])) : ?>
            <tr>
                <td style=" text-align: right; color:#666">発行日</td>
                <td style="padding-left:2em; text-align: right"><?php echo $item['issue_date'] ?? ''; ?></td>
            </tr>
        <?php endif ?>
    </table>
</div>
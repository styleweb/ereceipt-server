    <table class="items" style="width:100%; border-collapse: collapse">
        <tr>
            <th class="al">品目</th>
            <th class="ac"></th>
            <th class="ac">数量</th>
            <th class="ar">単価</th>
            <th class="ar">金額</th>
        </tr>
        <tr>
            <td colspan="5" style="padding: 0">
                <div style="border-bottom: 1px solid #ccc"></div>
            </td>
        </tr>
        <?php foreach ($item['items'] as $row) : ?>
            <tr>
                <td>
                    <?php echo $row['title'] ?? ''; ?>
                    <?php if ($row['detail_enable']) : ?>
                        <div style="font-size: 12px; color:#666; line-height:1.1">
                            <?php echo nl2br($row['detail']) ?? ''; ?>
                        </div>
                    <?php endif ?>
                </td>
                <td style="width: 10px"><?php echo $row['tax_rate'] === 8 ? '※' : ''; ?></td>
                <td class="ac" style="width: 50px;">
                    <?php echo $row['quantity'] ?? ''; ?>
                    <?php echo $row['unit'] ?? ''; ?>
                </td>
                <td class="ar" style="width: 90px;">&yen;<?php echo number_format($row['price']) ?? ''; ?></td>
                <td class="ar" style="width: 100px;">&yen;<?php echo number_format($row['amount']) ?? ''; ?></td>
            </tr>
            <tr>
                <td colspan="5" style="padding: 0">
                    <div style="border-bottom: 1px solid #ccc"></div>
                </td>
            </tr>
        <?php endforeach ?>
        <?php
        if (!empty($item['children'])) :
            foreach ($item['children'] as $child) :
                echo '<tr><td colspan="4" style="font-size:x-small; padding:20px 0 0 0;">' . $child['issue_date'] . 'のご請求を合算</td></tr>';

                foreach ($child['items'] as $row2) :
        ?>
                    <tr>
                        <td>
                            <?php echo $row2['title'] ?? ''; ?>
                            <?php if ($row2['detail_enable']) : ?>
                                <div style="font-size: 12px; color:#666; line-height:1.1">
                                    <?php echo nl2br($row2['detail']) ?? ''; ?>
                                </div>
                            <?php endif ?>
                        </td>
                        <td style="width: 10px"><?php echo $row2['tax_rate'] === 8 ? '※' : ''; ?></td>
                        <td class="ac" style="width: 50px;">
                            <?php echo $row2['quantity'] ?? ''; ?>
                            <?php echo $row2['unit'] ?? ''; ?>
                        </td>
                        <td class="ar" style="width: 90px;">&yen;<?php echo number_format($row2['price']) ?? ''; ?></td>
                        <td class="ar" style="width: 100px;">&yen;<?php echo number_format($row2['amount']) ?? ''; ?></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 0">
                            <div style="border-bottom: 1px solid #ccc"></div>
                        </td>
                    </tr>

        <?php endforeach;
            endforeach;
        endif ?>
    </table>
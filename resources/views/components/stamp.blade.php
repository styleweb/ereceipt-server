<div style="text-align: right;">
    <?php if ($item['stamp']['company_image_path']) : ?>
        <img style="width: 250px;" src="{{ storage_path('app/public/') }}<?= $item['stamp']['company_image_path'] ?>">
    <?php endif ?>
    <table>
        <tr>
            <td>
                <div style="line-height: 1.1; font-size:11px;">
                    <?php foreach ($item['stamp']['details'] as $detail) { ?>
                        <?= $detail['title'] ?> <?= $detail['content'] ?><br>
                    <?php } ?>
                </div>
            </td>
            <td>
                <?php if ($item['stamp']['stamp_type'] == 1) : ?>
                    <?php if (!empty($item['stamp']['stamp_name'])) : ?>
                        <img style="width: 75px;" src="<?= "https://ereceipt-server.stylesv.net/stamp/{$item['stamp']['stamp_shape']}/{$item['stamp']['stamp_name']}" ?>" alt="">
                    <?php endif ?>
                <?php elseif ($item['stamp']['stamp_type'] == '2') : ?>
                    <?php if (!empty($item['stamp']['stamp_image_path'])) : ?>
                        <img style="width: 80px;" src="{{ storage_path('app/public/') }}<?= $item['stamp']['stamp_image_path'] ?>" alt="">
                    <?php endif ?>
                <?php endif ?>
            </td>
        </tr>
    </table>
</div>
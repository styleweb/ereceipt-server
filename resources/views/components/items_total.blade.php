<div class="cf">

    <?php if ($item['sub_total_8'] > 0) : ?>
        <div style="width: 300px; float:left; color:#666">
            <p style="margin:1em 0">※は軽減税率対象です</p>

            <p>内訳</p>
            <table>
                <tr>
                    <td>10%対象 &yen; <span style="color:#000"><?php echo number_format($item['sub_total_main']) ?? ''; ?></span></td>
                    <td style="padding-left:1em">
                        消費税
                        <?php if ($item['tax_type'] == 1) echo "(" ?>
                        &yen; <span style="color:#000"><?php echo number_format($item['tax_main']) ?? ''; ?></span>
                        <?php if ($item['tax_type'] == 1) echo ')' ?>
                    </td>
                </tr>
                <tr>
                    <td> 8%対象 &yen; <span style="color:#000"><?php echo number_format($item['sub_total_8']) ?? ''; ?></span></td>
                    <td style="padding-left:1em">
                        消費税
                        <?php if ($item['tax_type'] == 1) echo "(" ?>
                        &yen; <span style="color:#000"><?php echo number_format($item['tax_8']) ?? ''; ?></span>
                        <?php if ($item['tax_type'] == 1) echo ')' ?>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif ?>
    <table class="items" style="width: 300px; float:right;">
        <tr>
            <td class="ar">小計</td>
            <td class="ar" style="width: 100px;">&yen;<?php echo number_format($item['sub_total']) ?? ''; ?></td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0">
                <div style="border-bottom: 1px solid #ccc"></div>
            </td>
        </tr>
        <tr>
            <td class="ar">消費税 <?php if ($item['sub_total_8'] < 1) echo ' 10%' ?></td>
            <td class="ar" style="width: 100px;">
                <?php if ($item['tax_type'] == 1) echo "(" ?>
                &yen;<?php echo number_format($item['tax']) ?? ''; ?>
                <?php if ($item['tax_type'] == 1) echo ')' ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0">
                <div style="border-bottom: 1px solid #ccc"></div>
            </td>
        </tr>
        <tr>
            <td class="ar">合計</td>
            <td class="ar" style="font-weight: bold; width: 100px;">&yen;<?php echo number_format($item['total']) ?? ''; ?></td>
        </tr>
    </table>
</div>
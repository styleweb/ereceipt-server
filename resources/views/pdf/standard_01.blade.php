<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{public_path('css/pdf/default.css')}}">
    <title><?php echo $title ?></title>
    <style type="text/css">
        #content {
            padding: 0 70px;
        }

        body {
            font-family: mp;
            color: #333;
        }

        .en {
            font-family: "sans-serif";
        }
    </style>
</head>
<div id="content">
    <div class="header ac">
        <div style="font-size: 24px; padding:50px 0 30px"><?php echo $item['title'] ?? ''; ?></div>
    </div>

    <table style="margin-bottom: 20px; width: 100%; border-collapse: collapse;">
        <tr>
            <td style="width:60%">
                @component('components.customer')
                @slot('item', $item)
                @endcomponent
            </td>
            <td style="width:40%">
                @component('components.date')
                @slot('item', $item)
                @endcomponent
            </td>
        </tr>
    </table>

    <div class="cf" style="margin-bottom: 20px">
        <div style="float:left; width: 50%">
            <div style="margin-bottom: 15px;">
                <p style="margin-bottom: 15px;"><?php echo $item['text'] ?? ''; ?></p>
                @component('components.total')
                @slot('item', $item)
                @endcomponent
            </div>
            <div>
                <?php if (in_array($item['type'], ['estimate', 'invoice'])) :
                    $expire_title = $item['type'] == 'estimate' ? '有効期限' : '支払期限';
                ?>
                    @component('components.expire')
                    @slot('item', $item)
                    @slot('title', $expire_title)
                    @endcomponent
                <?php endif ?>
                @component('components.details')
                @slot('item', $item)
                @endcomponent
            </div>
        </div>
        <div style="float:right;">
            @component('components.stamp')
            @slot('item', $item)
            @endcomponent
        </div>
    </div>
    <div style="margin-bottom: 20px;">
        @component('components.items')
        @slot('item', $item)
        @endcomponent

        @component('components.items_total')
        @slot('item', $item)
        @endcomponent
    </div>
    <div>
        @component('components.remarks')
        @slot('item', $item)
        @endcomponent
    </div>
</div>


</body>


</html>
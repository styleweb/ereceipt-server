<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contract;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Contract::query();
        $fieldes = ['state', 'contract_id', 'customer_id'];
        foreach ($fieldes as $field) {
            $value = $request->input($field);

            if (!is_null($value)) {
                if (is_array($value)) {
                    $query->where($field, $value[1], $value[0]);
                } else {
                    $query->where($field, $value);
                }
            }
        }

        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            $query->where(function ($query) use ($word) {
                $query->where('title', "LIKE", "%$word%")
                    ->orWhereHas('customer', function ($q) use ($word) {
                        return $q->where('name', "LIKE", "%$word%");
                    });
            });
            return $query;
        });

        $sort_by = $request->input('sort_by');
        $sort_desc = $request->input('sort_desc') === 'true' ? true : false;
        $sort = $sort_desc ? 'desc' : 'asc';
        if ($sort_by) {
            $query->orderBy($sort_by, $sort);
        } else {
            $query->orderBy('end_date', $sort)->orderBy('id', $sort);
        }

        $per_page = $request->input('per_page') ?: 20;

        return $query
            ->with('customer')
            ->with('delivery')
            ->with('estimate')
            ->with('invoice')
            ->with('receipt')
            ->with('estimate_template')
            ->with('delivery_template')
            ->with('invoice_template')
            ->with('receipt_template')
            ->with('estimate_mail_template')
            ->with('delivery_mail_template')
            ->with('invoice_mail_template')
            ->with('receipt_mail_template')
            ->paginate($per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 契約更新の場合
        if (isset($request->renewal_id) && $request->renewal_id) {
            $item = Contract::find($request->renewal_id);
            $item->state = 0;
            $item->save();
        }

        return Contract::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $item = Contract::find($id);
        // $item->name = $request->name;
        // $item->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Contract::where('id', $id)->first();
        return $item->delete();
    }
}

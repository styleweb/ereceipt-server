<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;
use App\Models\DocumentTemplate;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{

    protected $documentType;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // estimate, invoice, delivery, receipt のどれかを取得
            $path = $request->path();
            $segments = explode('/', $path);
            $this->documentType = $segments[1]; // api/以降を取得
            return $next($request);
        });
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Document::query();

        $query->where('type', $this->documentType);

        $fieldes = ['state', 'is_contract', 'customer_id', 'parent_id'];
        foreach ($fieldes as $field) {
            $value = $request->input($field);

            if (!is_null($value)) {
                if (is_array($value)) {
                    $query->where($field, $value[1], $value[0]);
                } else {
                    $query->where($field, $value);
                }
            }
        }

        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            return $query->where('superscription', "LIKE", "%$word%")->orWhere('number', "LIKE", "%$word%")->orWhere('details', "LIKE", "%$word%");
        });

        $per_page = $request->input('per_page') ?: 20;

        return $query->orderBy('id', 'desc')
            ->with('customer')
            ->with('delivery')
            ->with('estimate')
            ->with('invoice')
            ->with('receipt')
            ->with('mails')
            ->with('mail')
            ->paginate($per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        if ($request->save_template) {
            DocumentTemplate::create(
                $request->toArray()
            );
        }

        return Document::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        return Document::where('uuid', $uuid)->with('children')->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $item = Document::find($id);
        // $item->name = $request->name;
        // $item->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Document::where('id', $id)->first();
        return $item->delete();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdf($document_type, $uuid)
    {
        $item = Document::where('uuid', $uuid)->where('type', $document_type)->first();

        if ($item) {
            $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

            $title = self::makePdfTitle($document_type, $item);

            $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
            $pdf->setPaper('A4');
            return $pdf->stream($title);
        } else {
            return abort(404);
        }
    }

    public function public($document_type, $uuid)
    {
        $item = Document::withoutGlobalScope('own')->where('uuid', $uuid)->first();

        if ($item) {
            if (!Auth::check()) {
                $this->addLog($item);

                // $session_key = "{$document_type}_{$uuid}";
                // // sessionが存在しないなら追加
                // if (session()->exists($session_key)) {
                //     // 前回アクセスが、30分より前なら追加
                //     $access = session()->get($session_key);
                //     if ($access < strtotime("-5 min")) {
                //         $this->addLog($item);
                //     }
                // } else {
                //     $this->addLog($item);
                // }
            }


            $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

            $title = self::makePdfTitle($document_type, $item);

            $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
            $pdf->setPaper('A4');
            return $pdf->stream($title);
        } else {
            return abort(404);
        }
    }

    static function makePdfTitle(String $document_type, Document $item)
    {
        $title = $item->title . '.pdf';
        // switch ($document_type) {
        //     case 'estimate':
        //         $title = '見積書.pdf';
        //         break;
        //     case 'invoice':
        //         $title = '請求書.pdf';
        //         break;
        //     case 'delivery':
        //         $title = '納品書.pdf';
        //         break;
        //     case 'receipt':
        //         $title = '領収書.pdf';
        //         break;
        //     default:
        //         break;
        // }

        foreach ($item->details as $row) {
            if ($row['title'] === '件名' && !empty($row['content'])) {
                $title = $row['content'] . '_' . $title;
            }
        }

        if ($item->number) {
            $title = $item->number . '_' . $title;
        }

        return $title;
    }

    protected function addLog($item)
    {
        // $session_key = "estimate_{$item->id}";
        $logs = $item->logs ?? [];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        $data = [
            'date' => date('Y/n/j H:i:s'),
            'ip' => $ip_address,
            'user_agent' => $_SERVER["HTTP_USER_AGENT"]
        ];
        // Log::debug($logs);
        array_unshift($logs, $data);
        $item->logs = $logs;
        $item->save();
        // session()->put($session_key, time());
    }

    public function csv()
    {
        // コールバック関数に１行ずつ書き込んでいく処理を記述
        $callback = function () {
            // 出力バッファをopen
            $stream = fopen('php://output', 'w');
            // 文字コードをShift-JISに変換
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');
            // ヘッダー行
            fputcsv($stream, [
                'ID',
            ]);
            // データ
            $items = Document::orderBy('id', 'desc');
            // ２行目以降の出力
            // cursor()メソッドで１レコードずつストリームに流す処理を実現できる。
            foreach ($items->cursor() as $item) {
                fputcsv($stream, [
                    $item->id,
                ]);
            }
            fclose($stream);
        };

        // 保存するファイル名
        $filename = sprintf('お見積り-%s.csv', date('Ymd'));

        // ファイルダウンロードさせるために、ヘッダー出力を調整
        $header = [
            'Content-Type' => 'application/octet-stream',
        ];

        return response()->streamDownload($callback, $filename, $header);
    }


    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function get_template($id = null)
    // {
    //     if (is_null($id)) {
    //         $id = Auth::user()->group->template_default->invoice;
    //         Log::debug($id);
    //     }
    //     $item = DocumentTemplate::where('id', $id)->first();
    //     if (!empty($item)) {
    //         $item->setHidden(['id']);
    //         return $item;
    //     }
    //     return;
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function get_copy($id = null)
    // {
    //     $item =
    //         Document::with('group')->where('group_id', Auth::user()->group_id)->where('id', $id)->first();
    //     $item->setHidden(['id']);

    //     return $item;
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function from_estimate($id)
    // {
    //     $defaultTemplateId = Auth::user()->group->invoice_template_id;
    //     $item = DocumentTemplate::where('group_id', Auth::user()->group_id)->where('id', $defaultTemplateId)->first();
    //     $item->setHidden(['id']);

    //     $estimate =
    //         Estimate::with('group')->where('group_id', Auth::user()->group_id)->where('id', $id)->first();

    //     $item->superscription  = $estimate->superscription;
    //     $item->items = $estimate->items;
    //     $item->sub_total = $estimate->sub_total;
    //     $item->total = $estimate->total;
    //     $item->design = $estimate->design;

    //     // if ($item) {
    //     //     unset($item->id);
    //     //     unset($item->title);
    //     //     unset($item->text);
    //     //     unset($item->number);
    //     //     unset($item->issue_date);
    //     //     unset($item->expire_date);
    //     //     unset($item->remarks);
    //     // }

    //     return $item;
    // }


    public function replace_number($number = '')
    {
        $item = Document::createNumberFromFormat(
            Auth::check() ? Auth::user()->group->invoice_numbering : null,
            $number,
        );
        return $item;
    }
}

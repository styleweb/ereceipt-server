<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GuestDocument;
use Barryvdh\DomPDF\Facade\Pdf;

class GuestDocumentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = GuestDocument::create(
            $request->toArray()
        );
        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        return GuestDocument::where('uuid', $uuid)->with('children')->first();
    }


    public function public($document, $uuid)
    {
        $item = GuestDocument::where('uuid', $uuid)->first();

        if ($item) {

            $design = in_array($item->design, ['cool_01', 'elegant_01', 'standard_01']) ? $item->design : 'standard_01';

            $title = self::makePdfTitle($document, $item);

            $pdf = PDF::loadView("pdf/{$design}", ['item' => $item, 'title' => $title]);
            $pdf->setPaper('A4');
            return $pdf->stream($title);
        } else {
            return abort(404);
        }
    }

    static function makePdfTitle(String $document_type, GuestDocument $item)
    {
        $title = $item->title . '.pdf';


        foreach ($item->details as $row) {
            if ($row['title'] === '件名' && !empty($row['content'])) {
                $title = $row['content'] . '_' . $title;
            }
        }

        if ($item->number) {
            $title = $item->number . '_' . $title;
        }

        return $title;
    }




    // public function replace_number($number = '')
    // {
    //     $item = GuestDocument::createNumberFromFormat(
    //         $number,
    //     );
    //     return $item;
    // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DocumentTemplate;
use Illuminate\Support\Facades\Auth;

class DocumentTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null, Request $request)
    {
        $query = DocumentTemplate::query();

        if ($type) {
            $query->where('type', $type);
        }

        $fieldes = ['is_contract'];
        foreach ($fieldes as $field) {
            $value = $request->input($field);
            if (!is_null($value)) {
                if (is_array($value)) {
                    $query->where($field, $value[1], $value[0]);
                } else {
                    $query->where($field, $value);
                }
            }
        }

        // 検索
        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            return $query->where('name', "LIKE", "%$word%")->orWhere('id', $word);
        });
        return $query->orderBy('created_at', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        return DocumentTemplate::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DocumentTemplate::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = DocumentTemplate::where('group_id', Auth::user()->group_id)->find($id);
        return $item->delete();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $query = DocumentTemplate::query();


        $type = $request->input('type');
        if ($type) {
            $query->where('type', $type);
        }

        $is_contract = $request->input('is_contract');
        if ($is_contract) {
            $query->where('is_contract', $is_contract);
        }

        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            return $query->where('name', "LIKE", "%$word%");
        });

        $id = $request->input('id', 0);
        if ($id > 0) {
            $query->orderByRaw(
                'id = ? DESC',
                [$id]
            );
        }

        $items = $query->take(10)->get();

        return $items;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function get_group()
    // {
    //     $item = Group::where('id', Auth::user()->group_id)->first();

    //     if ($item) {
    //         unset($item->id);
    //         unset($item->name);
    //     }
    //     return $item;
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function get_copy($id = null)
    // {
    //     $item =
    //         DocumentTemplate::with('group')->where('group_id', Auth::user()->group_id)->where('id', $id)->first();

    //     if ($item) {
    //         unset($item->id);
    //         $item->information = $item->group->information;
    //         $item->stamp_name = $item->group->stamp_name;
    //     }
    //     return $item;
    // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MailTemplate;
use Illuminate\Support\Facades\Auth;

class MailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null, Request $request)
    {
        $query = MailTemplate::query();

        if ($type) {
            $query->where('type', $type);
        }

        $fieldes = ['is_contract'];
        foreach ($fieldes as $field) {
            $value = $request->input($field);

            if (!is_null($value)) {
                if (is_array($value)) {
                    $query->where($field, $value[1], $value[0]);
                } else {
                    $query->where($field, $value);
                }
            }
        }

        $type = $request->input('type');
        $query->when($type, function ($query, $type) {
            return $query->where('type', $type);
        });

        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            return $query->where('superscription', "LIKE", "%$word%")->orWhere('number', "LIKE", "%$word%")->orWhere('details', "LIKE", "%$word%");
        });

        return $query->orderBy('id', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'group_id' => Auth::user()->group_id,
            'user_id' => Auth::id()
        ]);

        return MailTemplate::updateOrCreate(
            ['id' => $request->id],
            $request->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return MailTemplate::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = MailTemplate::find($id);
        return $item->delete();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $query = MailTemplate::query();

        $type = $request->input('type');
        if ($type) {
            $query->where('type', $type);
        }

        $word = $request->input('word');
        $query->when($word, function ($query, $word) {
            return $query->where('name', "LIKE", "%$word%");
        });

        $is_contract = $request->input('is_contract');
        if ($is_contract) {
            $query->where('is_contract', $is_contract);
        }

        $id = $request->input('id', 0);
        if ($id > 0) {
            $query->orderByRaw(
                'id = ? DESC',
                [$id]
            );
        }

        $items = $query->take(10)->get();

        return $items;
    }
}

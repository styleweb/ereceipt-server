<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mail;
use App\Models\Document;
use Illuminate\Support\Facades\Auth;
use App\Mail\SendDocument;
use Illuminate\Support\Facades\Mail as Email;
use Illuminate\Support\Facades\DB;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $word = $request->input('word');
        $query = Mail::query();
        $query->when($word, function ($query, $word) {
            return $query->where('subject', "LIKE", "%$word%")->orWhere('id', $word);
        });
        return $query->orderBy('created_at', 'desc')->orderBy('id', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(
            function () use ($request) {
                $array = $request->toArray();
                $item = Mail::updateOrCreate(
                    ['id' => $request->id],
                    $array
                );
                if ($item->is_send === 1) {
                    EMail::to($item->to_address)->send(new SendDocument($item, Auth::user()->group));
                }
            }
        );
    }

    public function message($uuid)
    {
        $item = Mail::withoutGlobalScope('own')->where('uuid', $uuid)->first();

        if ($item) {
            if (!Auth::check()) {
                $this->addLog($item);
            }
            return $item;
        } else {
            return abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        return Mail::where('uuid', $uuid)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request)
    // {

    //     $groupId = Auth::user()->group_id;
    //     $item = Mail::find($groupId);

    //     return $item->update(
    //         $request->toArray()
    //     );
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Mail::find($id);
        return $item->delete();
    }


    protected function addLog($item)
    {
        $logs = $item->logs ?? [];
        // Proxyの為、IPは下記のいずれかで取得
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        $data = [
            'date' => date('Y/n/j H:i:s'),
            'ip' => $ip_address,
            'user_agent' => $_SERVER["HTTP_USER_AGENT"]
        ];
        array_unshift($logs, $data);
        $item->logs = $logs;
        $item->save();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class PdfController extends Controller
{

    public function test($id)
    {
        $pdf = PDF::loadView($id);
        $pdf->setPaper('A4');
        return $pdf->stream();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function receipt()
    {
        $sushiTable = [
            'たまご' => '100円',
            'いくら' => '200円',
            'サーモン' => '180円',
            'いか' => '100円',
            'マグロ' => '110円',
            'えび' => '100円',
        ];

        $pdf = PDF::loadView('receipt', ['sushiTable' => $sushiTable]);
        $pdf->setPaper('A4');
        return $pdf->stream();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function invoice()
    {
        $sushiTable = [
            'たまご' => '100円',

        ];

        $pdf = PDF::loadView('invoice', ['sushiTable' => $sushiTable]);
        $pdf->setPaper('A4');
        return $pdf->stream();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estimate()
    {
        $item = [
            'amount' => '100円',
        ];

        $pdf = PDF::loadView('estimate', ['item' => $item]);
        $pdf->setPaper('A4');
        return $pdf->stream();
    }

    public function image()
    {
        response('base64_decode("test")')->header('Content-type', 'image/png');
    }
}

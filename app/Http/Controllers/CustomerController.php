<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Imports\CustomerImport;
use Illuminate\Support\Facades\Log;

use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Customer::query();

        $word = $request->input('word');

        // 検索
        $query->when($word, function ($query, $word) {
            return $query->where('name', "LIKE", "%$word%");
        });

        $per_page = $request->input('per_page') ?: 20;

        return $query->orderBy('id', 'desc')->withCount('invoices')->paginate($per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array = $request->toArray();
        $array['group_id'] = Auth::user()->group_id;
        return Customer::updateOrCreate(
            ['id' => $request->id],
            $array
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group_id = Auth::user()->group_id;
        return Customer::where('group_id', $group_id)->where('id', $id)->first();;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Customer::where('group_id', Auth::user()->group_id)->find($id);
        return $item->delete();
    }


    /**
     * CSVインポート
     */
    protected function importCsv()
    {
        // アップロードされたCSVファイル
        $file = request()->file('files');

        Log::debug($file);

        try {
            $import = new CustomerImport();
            Excel::import($import, $file[0]);
        } catch (ValidationException $e) {
            Log::alert($e->errors());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {

        $query = Customer::query();

        // $fieldes = ['id'];
        // foreach ($fieldes as $field) {
        //     $value = $request->input($field);

        //     if (!is_null($value)) {
        //         if (is_array($value)) {
        //             $query->where($field, $value[1], $value[0]);
        //         } else {
        //             $query->where($field, $value);
        //         }
        //     }
        // }

        $word = $request->input('word');
        $id = $request->input('id', 0);

        $query->when($word, function ($query, $word) {
            return $query->orWhere('name', "LIKE", "%$word%");
        });

        if ($id > 0) {
            $query->orderByRaw(
                'id = ? DESC',
                [$id]
            );
        }

        return $query->take(10)->orderBy('id', 'desc')->get();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->query('tag')) {
            return File::where('tag', $request->query('tag'))->get();
        } else {
            return File::get();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();;
                $extension = $file->getClientOriginalExtension();
                $size = $file->getSize();
                $path = $file->store('file');
                $uploadFile = [
                    'name' => $name,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension,
                    'tag' => $request->tag ?? null,
                ];
                File::create($uploadFile);
            }
        }
        return true;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function message_file_upload(Request $request)
    {
        // \Log::debug($request);
        if ($request->file('image')) {
            $file = $request->file('image');
            $name = $file->getClientOriginalName();;
            $extension = $file->getClientOriginalExtension();
            $size = $file->getSize();
            $path = $file->store('file');
            $uploadFile = [
                'name' => $name,
                'path' => $path,
                'size' => $size,
                'extension' => $extension,
                'tag' => $request->tag ?? null,
            ];
            File::create($uploadFile);
        }

        $response = [
            'success' => 1,
            'file' => [
                'url' => config('app.url') . '/server/storage/' . $path,
                // 'url' => 'https://placehold.co/200x200',
            ]
        ];

        // Json形式で返却
        return response()->json($response);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = "public/file/{$id}";
        $file = Storage::get($path);
        $mimeType = Storage::mimeType($path);

        return response()->make($file, 200, [
            'Content-Type'        => $mimeType,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return File::where('id', $id)->update(['name' => $request->name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null)
    {
        $item = File::find($id);
        $item->delete();
        Storage::delete($item->name);
        return [];
    }

    public function multiDelete(Request $request)
    {
        $item = File::find($request->id);
        $item->delete();
        Storage::delete($request->path);
        return [];
    }
}

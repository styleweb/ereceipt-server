<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    use Notifiable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $word = $request->input('word');
        $query = User::query();
        $query->when($word, function ($query, $word) {
            return $query->where('subject', "LIKE", "%$word%")->orWhere('id', $word);
        });
        $query->where('group_id', Auth::user()->group_id);
        return $query->orderBy('created_at', 'desc')->orderBy('id', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->id) {
            // 編集
            $item = User::where('group_id', Auth::user()->group_id)->find($request->id);

            // requestで送られてきたものだけをsaveする
            \Log::debug($request->all());
            $item->fill($request->all())->save();
        } else {
            // 新規作成
            $item = new User();
            $item->name = $request->name;
            $item->email = $request->email;
            $item->group_id = Auth::user()->group_id;
            $item->password = Hash::make($request->password);
            $item->save();
        }
        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     return Receipt::find($id);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $item = User::where('group_id', Auth::user()->group_id)->find(Auth::id());
        $item->name = $request->name;
        $item->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User::where('group_id', Auth::user()->group_id)->find($id);
        return $item->delete();
    }

    public function changeStatus(Request $request)
    {
        // if ($request->type === 'subscription.resumed') {
        //     Log::debug('再開だべ');
        //     $pause = false;
        // }

        // if ($request->type === 'subscription.paused') {
        //     Log::debug('停止だべ');
        //     $pause = true;
        // }

        // if (isset($request->data['customer']) && is_bool($pause)) {
        //     $item = User::where('credit_customer_id', $request->data['customer'])->first();
        //     $item->paused = $pause;
        //     $item->save();
        // }
    }
}

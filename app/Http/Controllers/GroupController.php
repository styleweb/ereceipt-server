<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class GroupController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // メールパスワードが空の場合は除外
        if (empty($request->mail_password)) {
            $request->offsetUnset('mail_password');
        }

        $array = $request->toArray();
        return Group::updateOrCreate(
            ['id' => Auth::user()->group_id],
            $array
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $groupId = Auth::user()->group_id;
        return Group::find($groupId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $groupId = Auth::user()->group_id;
        $item = Group::find($groupId);

        return $item->update(
            $request->toArray()
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        if (Auth::user()->admin_enable) {
            $groupId = Auth::user()->group_id;
            $item = Group::find($groupId);
            return $item->delete();
        } else {
            return abort(405);
        }
    }
}

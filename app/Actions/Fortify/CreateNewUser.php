<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\Group;
use App\Models\DocumentTemplate;
use App\Models\MailTemplate;
use App\Models\Customer;
use App\Models\Contract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        $userData = [
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ];

        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ])->validate();

        // 新規の登録の場合はメンバー追加のみ
        if (Auth::check()) {
            $userData['group_id'] = Auth::user()->group_id;
            return User::create($userData);
        }

        ///// 新規登録

        // Group
        $groupData = $input['group'];

        $groupData['remarks'] = [
            'estimate' => [['title' => '', 'content' => '']],
            'delivery' => [['title' => '', 'content' => '']],
            'invoice' => [['title' => 'お振込み先', 'content' => "〇〇銀行　〇〇支店　普通口座　口座番号：0000000　口座名義：〇〇 \n ※振込手数料はお客様負担とさせていただきます。"]],
            'receipt' => [['title' => '', 'content' => '']]
        ];

        // $groupData['stamp']['details'][0] = ['title' => '', 'content' => $input['group']['name']];
        $groupData['stamp']['details'][] = ['title' => '登録番号', 'content' => '1234567890123'];
        $groupData['stamp']['details'][] = ['title' => '担当者', 'content' => $input['name']];
        // $groupData['stamp']['details'][] = ['title' => 'E-MAIL', 'content' => $input['email']];
        $groupData['stamp']['stamp_name'] = $input['group']['name'];
        $groupData['stamp']['stamp_type'] = 0;
        $groupData['stamp']['stamp_shape'] = 2;
        $groupData['stamp']['stamp_image_path'] = '';
        $groupData['stamp']['company_image_path'] = 'file\/sample.png';


        $groupData['mail_footer'] =
            <<<EOM
            …━━━…‥・・・‥…━━━…・・‥…
            {グループ名}
            担当者: {ユーザー名}
            EMAIL: {ユーザーメールアドレス}
            …━━━…‥・・・‥…━━━…・・‥…
            EOM;


        $groupData['states'] = [
            'estimate' => [["id" => 1, "name" => '有効'], ["id" => 2, "name" => '無効']],
            'delivery' =>
            [["id" => 1, "name" => '有効'], ["id" => 2, "name" => '無効']],
            'invoice' =>
            [
                ["id" => 1, "name" => '請求中'],
                ["id" => 2, "name" => '入金確認済'],
                ["id" => 3, "name" => '保留中'],
                ["id" => 4, "name" => '無効']
            ],
            'receipt' =>
            [["id" => 1, "name" => '有効'], ["id" => 2, "name" => '無効']],
            'template' =>
            [["id" => 1, "name" => '有効'], ["id" => 2, "name" => '無効']],
            'contract' =>
            [["id" => 1, "name" => '有効'], ["id" => 2, "name" => '無効']],
            'customer' =>
            [["id" => 1, "name" => '有効'], ["id" => 2, "name" => '無効']],
        ];

        $groupData['default_queries'] = [
            'estimate' => '?state=1&contract_id=0',
            'delivery' => '?state=1&contract_id=0',
            'invoice' => '?state=1&contract_id=0',
            'receipt' => '?state=1&contract_id=0',
            'template' => '?state=1&contract_id=0',
            'contract' => '?state=1',
            'customer' => '?state=1'
        ];

        $group = Group::create($groupData);


        // User
        $userData['group_id'] = $group->id;
        $userData['admin_enable'] = 1;
        $user = User::create($userData);


        // Template
        $documents = config('app.documents');

        foreach ($documents as $document) {

            $documentAction = str_replace('書',  "", __("document.{$document}"));
            $template = [
                'group_id' => $group->id,
                'user_id' => $user->id,
                'name' => "通常{$documentAction}用",
                'type' => $document,
                'title' => __("document.{$document}"),
                'text' => __("document.{$document}_text"),
            ];
            $returnObj = DocumentTemplate::withoutGlobalScope('own')->create($template);
            $groupUpdateData[$document . '_template_id'] = $returnObj->id;
        }


        // Mail template
        foreach ($documents as $document) {
            $documentName = __("document.{$document}");
            $body = <<<EOM
            {宛名} {敬称}

            いつもお世話になっております。
            {$documentName}をご案内させていただきます。
            
            =======================================
            ◎{$documentName}
            {PDFURL}
            =======================================

            ご確認のほど、よろしくお願い致します。
            EOM;

            $mail_template = [
                'group_id' => $group->id,
                'user_id' => $user->id,
                'name' => __("document.{$document}") . '案内',
                'type' => $document,
                'subject' => '【' . __("document.{$document}") . '案内】{グループ名}',
                'body' => $body,
            ];
            $returnObj = MailTemplate::withoutGlobalScope('own')->create($mail_template);
            $groupUpdateData[$document . '_mail_template_id'] = $returnObj->id;
        }

        // Customer
        $customers = [
            [
                'group_id' => $group->id,
                'name' => '株式会社クライアント食品',
                'email' => 'syokuhin@sample.mypdf.jp',
            ],
            [
                'group_id' => $group->id,
                'name' => 'クライアント建築株式会社',
                'email' => 'kensetsu@sample.mypdf.jp',
            ]
        ];
        foreach ($customers as $key => $customer) {
            $resultCustomer = Customer::withoutGlobalScope('own')->create($customer);
        }


        // Template(Contract) 

        foreach ($documents as $document) {
            $documentAction = str_replace('書',  "", __("document.{$document}"));
            $template = [
                'group_id' => $group->id,
                'user_id' => $user->id,
                'name' => "保守契約{$documentAction}用",
                'type' => $document,
                'title' => __("document.{$document}"),
                'text' => __("document.{$document}_text"),
                'is_contract' => 1,
            ];
            $returnObj = DocumentTemplate::withoutGlobalScope('own')->create($template);
            $contractData[$document . '_template_id'] = $returnObj->id;
        }

        // MailTemplate(Contract) 
        $body = <<<EOM
            {宛名} {敬称}

            いつもお世話になっております。
            契約更新のご案内をさせていただきます。

            請求書記載の期限までにお振込みをお願いいたします。
            
            ◎現在のご契約
            =======================================
            【 内容 】 {契約名}
            【 期間 】 {開始日}～{終了日}
            =======================================
            
            ◎請求書
            =======================================
            {PDFURL}
            =======================================

            ご確認のほど、よろしくお願い致します。
            EOM;

        $mail_template = [
            'group_id' => $group->id,
            'user_id' => $user->id,
            'name' => '契約更新案内',
            'type' => 'contract',
            'subject' => '【{契約名}更新の案内】{グループ名}' . $group->name,
            'body' => $body,
        ];
        $returnObj = MailTemplate::withoutGlobalScope('own')->create($mail_template);
        $contractData['mail_template_id'] = $returnObj->id;


        // Contract
        $mergeContractData = [
            'group_id' => $group->id,
            'user_id' => $user->id,
            'customer_id' => $resultCustomer->id,
            'estimate_create_date' => date('Y-m-d'),
            'delivery_create_date' => date('Y-m-d'),
            'invoice_create_date' => date('Y-m-d'),
            'receipt_create_date' => date('Y-m-d'),
            'title' => 'ウェブホスティング契約',
            'cycle' => 'month',
            'start_date' => date('Y-m-d'),
            'end_date' => date('Y-m-d', strtotime('1 month -1 day')),
            'next_start_date' => date('Y-m-d', strtotime('1 month')),
            'next_end_date' => date('Y-m-d', strtotime('2 month -1 day')),
            'state' => 1,
        ];

        $resultContractData = array_merge($contractData, $mergeContractData);
        Contract::withoutGlobalScope('own')->create($resultContractData);


        // group update
        $group->update($groupUpdateData);
        return $user;
    }
}

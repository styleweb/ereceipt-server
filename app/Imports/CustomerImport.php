<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CustomerImport implements ToCollection, WithStartRow
{

    public static $startRow = 2;

    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            Customer::create([
                'name' => $row[0], // 行の1列目
                'email' => $row[1], // 行の2列目
                // 'password' => bcrypt($row[2]), // 行の3列目
            ]);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return self::$startRow;
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Swift_SmtpTransport;
use Swift_Mailer;
use Illuminate\Support\Facades\Mail;

class SendDocument extends Mailable
{
    use Queueable, SerializesModels;

    private $from_name;
    private $reply_to;
    private $body;
    private $content;

    private $mail_host;
    private $mail_port;
    private $mail_user_name;
    private $mail_password;
    private $mail_from_address;
    private $mail_encryption;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $group)
    {
        $this->request = $request;

        $this->subject = $request->subject;
        $this->body = $request->body;
        $this->from_name = $request->from_name ? $request->from_name : $group->name;
        $this->content = $request->content;

        $this->mail_host = config('mail.mailers.smtp.host');
        $this->mail_port = config('mail.mailers.smtp.port');
        $this->mail_user_name = config('mail.mailers.smtp.username');
        $this->mail_password = config('mail.mailers.smtp.password');
        $this->mail_from_address = config('mail.from.address');
        $this->mail_encryption = config('mail.mailers.smtp.encryption');


        if ($group && $group->uses_domain_mail) {
            $this->mail_host = $group->mail_host;
            $this->mail_port = $group->mail_port;
            $this->mail_user_name = $group->mail_user_name;
            $this->mail_password = $group->mail_password;
            $this->mail_from_address = $group->mail_from_address;
            $this->mail_encryption = $group->mail_encryption;
        } else {
            $this->reply_to = $group->mail_reply_to;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /** @var \Illuminate\Mail\Mailer $mailer */
        $mailer = Mail::mailer();
        $transport = new Swift_SmtpTransport($this->mail_host, $this->mail_port, $this->mail_encryption);
        $transport->setUsername($this->mail_user_name);
        $transport->setPassword($this->mail_password);
        $mailer->setSwiftMailer(new Swift_Mailer($transport));
        $mailer->alwaysFrom($this->mail_from_address, $this->from_name);

        if (!empty($this->reply_to)) {
            $this->replyTo($this->reply_to, $this->from_name);
        }

        // CC があれば設定
        if (isset($this->request->cc)) {
            $this->cc($this->request->cc);
        }

        // BCC があれば設定
        if (isset($this->request->bcc)) {
            $this->bcc($this->request->bcc);
        }

        // Click to Message が有効な場合は、本文を変更
        $body = $this->body;
        if ($this->request->is_click_to_message_enabled) {
            $body = $this->request->before_body;
        }

        return $this->subject($this->subject)
            ->text('emails.default')->with([
                'body' => $body,
            ]);
    }
}

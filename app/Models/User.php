<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\RegisterUserNotification;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'group_id',
        'default_queries',
        'admin_enable',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'default_queries' => 'json',
    ];


    /**
     * ユーザー情報にGroupを追加
     * @return string
     */
    protected $appends = ['group'];
    public function getGroupAttribute()
    {
        return Group::with('estimate_template')
            ->with('delivery_template')
            ->with('invoice_template')
            ->with('receipt_template')
            ->with('estimate_mail_template')
            ->with('delivery_mail_template')
            ->with('invoice_mail_template')
            ->with('receipt_mail_template')
            ->find($this->group_id);
    }

    /**
     * モデル起動時
     * @return void
     */
    protected static function booted()
    {

        // データ作成時
        static::creating(function (User $item) {
            $item->uuid = Str::uuid();
            return $item;
        });

        // 登録時にメール通知
        static::created(function (User $user) {
            if ($user->admin_enable) {
                $user->notify(new RegisterUserNotification($user));
            }

            // sequenceを追加
            $sequence = GroupSequence::firstOrCreate(['group_id' => $user->group_id], ['user_sequence' => 0]);
            $sequence->increment('user_sequence');
            $user->sequence = $sequence->user_sequence;
            $user->save();
        });
    }

    /**
     * パスワードリセット通知をユーザーに送信
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $url = config('app.site_url') . '/reset-password?token=' . $token;
        $this->notify(new ResetPasswordNotification($url));
    }
}

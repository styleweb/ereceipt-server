<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class Contract extends Model
{
    use HasFactory;

    protected $casts = ['estimate_mail_dates' => 'json', 'delivery_mail_dates' => 'json', 'invoice_mail_dates' => 'json', 'receipt_mail_dates' => 'json', 'variables' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'estimate_id',
        'invoice_id',
        'delivery_id',
        'receipt_id',
        'estimate_template_id',
        'delivery_template_id',
        'invoice_template_id',
        'receipt_template_id',
        'estimate_mail_template_id',
        'delivery_mail_template_id',
        'invoice_mail_template_id',
        'receipt_mail_template_id',
        'estimate_create_date',
        'delivery_create_date',
        'invoice_create_date',
        'receipt_create_date',
        'estimate_mail_dates',
        'delivery_mail_dates',
        'invoice_mail_dates',
        'receipt_mail_dates',
        'title',
        'cycle',
        'start_date',
        'end_date',
        'next_start_date',
        'next_end_date',
        'is_auto_document',
        'is_auto_email',
        'is_confirmation_email_disabled',
        'state',
        'canceled',
        'memo',
    ];

    /**
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (Contract $Contract) {
            $Contract->uuid = Str::uuid();
            if (Auth::check()) {
                $Contract->group_id = Auth::user()->group_id;
                $Contract->user_id = Auth::id();
            }
            return $Contract;
        });

        static::created(function (Contract $item) {
            $sequence = GroupSequence::firstOrCreate(['group_id' => $item->group_id], ['contract_sequence' => 0]);
            $sequence->increment('contract_sequence');
            $item->sequence = $sequence->contract_sequence;
            $item->save();
            return;
        });
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function delivery()
    {
        return $this->hasOne(Document::class, 'id', 'delivery_id')->where('type', 'delivery')->with('mails');
    }

    public function estimate()
    {
        return $this->hasOne(Document::class, 'id', 'estimate_id')->where('type', 'estimate')->with('mails');
    }

    public function invoice()
    {
        return $this->hasOne(Document::class, 'id', 'invoice_id')->where('type', 'invoice')->with('mails');
    }

    public function receipt()
    {
        return $this->hasOne(Document::class, 'id', 'receipt_id')->where('type', 'receipt')->with('mails');
    }

    public function estimate_template()
    {
        return $this->belongsTo(DocumentTemplate::class);
    }

    public function delivery_template()
    {
        return $this->belongsTo(DocumentTemplate::class);
    }

    public function invoice_template()
    {
        return $this->belongsTo(DocumentTemplate::class);
    }

    public function receipt_template()
    {
        return $this->belongsTo(DocumentTemplate::class);
    }

    public function estimate_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'estimate_mail_template_id');
    }

    public function delivery_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'delivery_mail_template_id');
    }

    public function invoice_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'invoice_mail_template_id');
    }

    public function receipt_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'receipt_mail_template_id');
    }
}

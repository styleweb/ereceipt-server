<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $casts = [
        'stamp' => 'json',
        'numbering' => 'json',
        'states' => 'json',
        'default_queries' => 'json',
        'remarks' => 'json',
    ];

    protected $fillable = [
        'name',
        'email',
        // 'zipcode',
        // 'address',
        // 'address2',
        // 'tel',
        // 'fax',
        // 'email',
        // 'web',
        'estimate_template_id',
        'invoice_template_id',
        'delivery_template_id',
        'receipt_template_id',
        'estimate_mail_template_id',
        'invoice_mail_template_id',
        'delivery_mail_template_id',
        'receipt_mail_template_id',
        'numbering',
        'mail_footer',

        'uses_domain_mail',
        'mail_reply_to',
        'mail_from_address',
        'mail_host',
        'mail_port',
        'mail_user_name',
        'mail_password',
        'mail_encryption',

        'states',
        'default_queries',
        'stamp',
        'remarks',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        // 'mail_password',
    ];

    // protected $appends = ['default_estimate_template', 'default_delivery_template', 'default_invoice_template', 'default_receipt_template', 'invoice_mail_template'];

    // public function getDefaultEstimateTemplateAttribute()
    // {
    //     return DocumentTemplate::where('id', $this->template_default['estimate'])->first();
    // }

    // public function getDefaultDeliveryTemplateAttribute()
    // {
    //     return DocumentTemplate::where('id', $this->template_default['delivery'])->first();
    // }

    // public function getDefaultInvoiceTemplateAttribute()
    // {
    //     return DocumentTemplate::where('id', $this->template_default['invoice'])->first();
    // }

    // public function getDefaultReceiptTemplateAttribute()
    // {
    //     return DocumentTemplate::where('id', $this->template_default['receipt'])->first();
    // }


    // public function getInvoiceMailTemplateAttribute()
    // {
    //     return MailTemplate::where('id', $this->mail_template_default['invoice'])->first();
    // }


    public static function boot()
    {
        parent::boot();

        static::deleting(function ($item) {
            $item->users()->delete();
            $item->customers()->delete();
            $item->files()->delete();
            $item->invoices()->delete();
            $item->invoice_templates()->delete();
        });
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function mail_templates()
    {
        return $this->hasMany(MailTemplate::class);
    }

    public function estimate_template()
    {
        return $this->hasOne(DocumentTemplate::class, 'id', 'estimate_template_id');
    }

    public function delivery_template()
    {
        return $this->hasOne(DocumentTemplate::class, 'id', 'delivery_template_id');
    }

    public function invoice_template()
    {
        return $this->hasOne(DocumentTemplate::class, 'id', 'invoice_template_id');
    }

    public function receipt_template()
    {
        return $this->hasOne(DocumentTemplate::class, 'id', 'receipt_template_id');
    }

    public function estimate_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'estimate_mail_template_id');
    }

    public function delivery_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'delivery_mail_template_id');
    }

    public function invoice_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'invoice_mail_template_id');
    }

    public function receipt_mail_template()
    {
        return $this->hasOne(MailTemplate::class, 'id', 'receipt_mail_template_id');
    }
}






/**
 * ユーザー情報にpayjpカスタマー情報を追加
 * @return string
 */
    // public function getCreditAttribute()
    // {
    //     $credit = [
    //         "default_card" => null,
    //         'cards' => [
    //             'count' => 0,
    //             'data' => []
    //         ],
    //         'subscriptions' => [
    //             'count' => 0,
    //             'data' => []
    //         ],
    //     ];

    //     // payjp から顧客を取得
    //     if (!empty($this->credit_customer_id)) {
    //         $ch = curl_init();
    //         curl_setopt($ch, CURLOPT_URL, "https://api.pay.jp/v1/customers/{$this->credit_customer_id}");
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, [
    //             'Content-Type' => 'application/x-www-form-urlencoded',
    //         ]);
    //         curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //         curl_setopt($ch, CURLOPT_USERPWD, 'sk_test_0f95f73d284652ec98aea89c');
    //         $response = curl_exec($ch);
    //         curl_close($ch);

    //         // \Log::debug($response);
    //         $result = json_decode($response);
    //         if (isset($result->error)) {
    //             // \Log::debug($result->error->message);
    //         } else {
    //             $credit = [
    //                 "default_card" => $result->default_card,
    //                 'cards' => [
    //                     'count' => $result->cards->count,
    //                     'data' => $result->cards->data,
    //                 ],
    //                 'subscriptions' => [
    //                     'count' => $result->subscriptions->count,
    //                     'data' => $result->subscriptions->data,
    //                 ]
    //             ];
    //         }
    //     }
    //     return $credit;
    // }
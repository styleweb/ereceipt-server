<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class DocumentTemplate extends Model
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'stamp' => 'json', 'remarks' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'sequence',
        'type',
        'title',
        'text',
        'superscription',
        'attention',
        'number',
        'issue_format',
        'expire_type',
        'expire_calc_method',
        'expire_format',
        'expire_days',
        'details',
        'items',
        'tax_rate',
        'tax_type',
        'tax_calc',
        'sub_total',
        'sub_total_main',
        'sub_total_8',
        'tax',
        'tax_main',
        'tax_8',
        'total',
        'uses_stamp',
        'stamp',
        'uses_remarks',
        'remarks',
        'design',
        'is_contract',
        // ここから下はテンプレートのみ
        'name',
    ];

    /**
     * @return void
     */
    protected static function booted()
    {

        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (DocumentTemplate $item) {
            $item->uuid = Str::uuid();
            return $item;
        });

        static::created(function (DocumentTemplate $item) {

            // sequenceを追加
            $sequence = GroupSequence::firstOrCreate(['group_id' => $item->group_id], ['document_template_sequence' => 0]);
            $sequence->increment('document_template_sequence');
            $item->sequence = $sequence->document_template_sequence;
            $item->save();
            return;
        });
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}

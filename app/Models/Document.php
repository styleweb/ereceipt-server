<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class Document extends Model
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'stamp' => 'json', 'remarks' => 'json', 'mails' => 'json', 'logs' => 'json', 'children' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'estimate_id',
        'invoice_id',
        'delivery_id',
        'receipt_id',
        'contract_id',
        'parent_id',
        'children',
        'type',
        'title',
        'text',
        'superscription',
        'attention',
        'number',
        'issue_date',
        'issue_format',
        'expire_date',
        'expire_format',
        'details',
        'tax_rate',
        'tax_type',
        'tax_calc',
        'items',
        'sub_total',
        'sub_total_main',
        'sub_total_8',
        'tax',
        'tax_main',
        'tax_8',
        'total',
        'stamp',
        'remarks',
        'logs',
        'design',
        'state',
        'is_contract',
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function estimate()
    {
        return $this->hasOne(Document::class, 'id', 'estimate_id')->where('type', 'estimate');
    }

    public function delivery()
    {
        return $this->hasOne(Document::class, 'id', 'delivery_id')->where('type', 'delivery');
    }

    public function invoice()
    {
        return $this->hasOne(Document::class, 'id', 'invoice_id')->where('type', 'invoice');
    }

    public function receipt()
    {
        return $this->hasOne(Document::class, 'id', 'receipt_id')->where('type', 'receipt');
    }

    public function mail()
    {
        return $this->hasOne(Mail::class, 'id', 'mail_id')->where('is_send', 0);
    }

    public function mails()
    {
        return $this->hasMany(Mail::class)->orderBy('created_at', 'desc')->where('is_send', 1);
    }

    /**
     * @return Array
     */
    public function updateNumbering($document_type, $group_id)
    {
        $group = Group::where('id', $group_id)->first();
        $numbering = $group->numbering[$document_type];

        $numbering['total'] = $numbering['total'] + 1;
        if ($numbering['year']['date'] == date('Y')) {
            $numbering['year']['count'] = $numbering['year']['count'] + 1;
        } else {
            $numbering['year']['count'] = 1;
            $numbering['year']['date'] = date('Y');
        }
        if ($numbering['month']['date'] == date('m')) {
            $numbering['month']['count'] = $numbering['month']['count'] + 1;
        } else {
            $numbering['month']['count'] = 1;
            $numbering['month']['date'] = date('m');
        }
        if ($numbering['day']['date'] == date('d')) {
            $numbering['day']['count'] = $numbering['day']['count'] + 1;
        } else {
            $numbering['day']['count'] = 1;
            $numbering['day']['date'] = date('d');
        }

        // merge
        $new = array_merge($group->numbering, [$document_type => $numbering]);
        $group->numbering = $new;
        $group->save();

        return $group->numbering[$document_type];
    }

    /**
     * @return string
     */
    static function createNumberFromFormat($numbering, $number, $issue_date = null, $customer_id = null)
    {
        $pattern1 = '/\[発行日:([ -~]*)\]/';
        if (preg_match($pattern1, $number, $matches1)) {
            $date_str = $matches1[1];
            $number = preg_replace($pattern1, date($date_str), $number);
        }
        $pattern2 = '/\[連番:(\w+),([0-9])\]/';
        if (preg_match($pattern2, $number, $matches2)) {
            $cycle = $matches2[1];
            $pad = $matches2[2];
            $num_str = str_pad($numbering[$cycle]['count'], $pad, '0', STR_PAD_LEFT);
            $number = preg_replace($pattern2, $num_str, $number);
        }
        return $number;
    }

    /**
     * @return void
     */
    protected static function booted()
    {
        if (!App::runningInConsole()) {
            static::addGlobalScope('own', function (Builder $builder) {
                $builder->where('group_id', Auth::user()->group_id);
            });
        }

        // データ作成時
        static::creating(function (Document $document) {
            $document->uuid = Str::uuid();
            return $document;
        });

        // データ作成後
        static::created(function (Document $document) {

            // 自動採番 groupの連番を更新する
            $numbering = $document->updateNumbering($document->type, $document->group_id);

            // 番号を作成
            $document->number = $document->createNumberFromFormat($numbering, $document->number, $document->issue_date, $document->customer_id);

            // sequenceを更新しセット
            $sequence = GroupSequence::firstOrCreate(['group_id' => $document->group_id], ['document_sequence' => 0]);
            $sequence->increment('document_sequence');
            $document->sequence = $sequence->document_sequence;

            // 帳票を再保存
            $document->save();

            // 外部キーが含まれる場合、親データのforeign_keyに追加
            $types = ['estimate', 'delivery', 'invoice', 'receipt'];
            foreach ($types as $type) {
                if (isset($document->{$type . '_id'})) {
                    $parent_document = Document::withoutGlobalScope('own')->find($document->{$type . '_id'});
                    if ($parent_document) {
                        $parent_document->{$document->type . '_id'} = $document->id;
                        $parent_document->save();
                    }
                }
            }

            // contract_idが含まれる場合、契約書の外部キーに追加
            if (isset($document->contract_id)) {
                $contract = Contract::withoutGlobalScope('own')->find($document->contract_id);
                if ($contract) {
                    $contract->{$document->type . '_id'} = $document->id;
                    $contract->save();
                }
            }
        });

        // 作成後or更新後
        static::saved(
            function (Document $document) {

                // 子請求の処理
                if ($document->type == 'invoice') {

                    // parent_idとして登録されているidを持つデータを取得
                    $children = Document::withoutGlobalScope('own')->where('parent_id', $document->id)->get();

                    if (!empty($children) && !empty($document->children)) {
                        // いったん全ての子請求のparent_idを0に戻す
                        foreach ($children as $child) {
                            $item = Document::withoutGlobalScope('own')->find($child->id);
                            $item->parent_id = 0;
                            $item->save();
                        }

                        // $document->childrenのparent_idを$document->idにする
                        foreach ($document->children as $child) {
                            $item = Document::withoutGlobalScope('own')->find($child['id']);
                            $item->parent_id = $document->id;
                            $item->save();
                        }
                    }
                }
            }
        );

        static::deleting(
            function (Document $document) {

                // 子請求の処理
                if ($document->type == 'invoice') {

                    // 親請求を削除する場合、子請求のparent_idを0にする

                    // 登録されているidをparent_idに持つデータを取得
                    $children = Document::where('parent_id', $document->id)->get();

                    // $childrenのparent_idを0に戻す
                    if (!empty($children)) {
                        foreach ($children as $child) {
                            $child->parent_id = 0;
                            $child->save();
                        }
                    }
                    // 子請求の削除は、フロントの仕様上できない。合算されている場合編集不可。
                }
            }
        );
    }
}

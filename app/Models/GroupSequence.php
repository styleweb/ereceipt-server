<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupSequence extends Model
{
    use HasFactory;

    protected $fillable = [
        'group_id',
        'contract_sequence',
        'customer_sequence',
        'document_sequence',
        'document_template_sequence',
        'file_sequence',
        'mail_sequence',
        'mail_sequence',
        'user_sequence',
    ];
}

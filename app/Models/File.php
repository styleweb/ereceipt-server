<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class File extends Model
{
    use HasFactory;

    const UPDATED_AT = null;

    protected $fillable = [
        'name',
        'path',
        'size',
        'extension',
        'tag',
        'group_id',
    ];

    /**
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // uuidとgroupIdを追加
        static::creating(function (File $item) {
            $item->uuid = Str::uuid();
            $item->group_id = Auth::user()->group_id;
            return $item;
        });

        static::created(function (File $item) {
            // sequenceを追加
            $sequence = GroupSequence::firstOrCreate(['group_id' => $item->group_id], ['file_sequence' => 0]);
            $sequence->increment('file_sequence');
            $item->sequence = $sequence->file_sequence;
            $item->save();
            return;
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class Mail extends Model
{
    use HasFactory;

    protected $casts = ['to_address' => 'json', 'cc' => 'json', 'bcc' => 'json', 'content' => 'json', 'logs' => 'json'];

    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'document_id',
        'contract_id',
        'from_name',
        'reply_to',
        'to_address',
        'cc',
        'bcc',
        'subject',
        'is_click_to_message_enabled',
        'is_html_mail_enabled',
        'before_body',
        'body',
        'content',
        'is_send',
        'logs',
    ];

    /**
     * @return void
     */
    protected static function booted()
    {

        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (Mail $item) {
            $item->uuid = Str::uuid();

            if (!App::runningInConsole()) {
                $item->group_id = Auth::user()->group_id;
                $item->user_id = Auth::id();
            }

            // メッセージURLを置換
            if ($item->is_click_to_message_enabled) {
                $site_url = config('app.site_url');
                $mail_url = "{$site_url}/message/{$item->uuid}";
                $words = [
                    '/\{メッセージURL\}/' => $mail_url,
                ];
                $pattern = array_keys($words);
                $replace = array_values($words);
                $replace_before_body = preg_replace($pattern, $replace, $item->before_body);
                $item->before_body = $replace_before_body;
            }
            return $item;
        });

        // データ作成後
        static::created(function (Mail $item) {

            // sequenceを追加
            $sequence = GroupSequence::firstOrCreate(['group_id' => $item->group_id], ['mail_sequence' => 0]);
            $sequence->increment('mail_sequence');
            $item->sequence = $sequence->mail_sequence;
            $item->save();

            // // document_idが含まれる場合
            if (isset($item->document_id)) {
                $document = Document::find($item->document_id);
                $document->mail_id = $item->id;
                $document->save();
            }
        });
    }
}

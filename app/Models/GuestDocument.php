<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class GuestDocument extends Model
{
    use HasFactory;

    protected $casts = ['items' => 'json', 'details' => 'json', 'stamp' => 'json', 'remarks' => 'json'];

    protected $fillable = [

        'uuid',
        'type',
        'title',
        'text',
        'superscription',
        'attention',
        'number',
        'issue_date',
        'issue_format',
        'expire_date',
        'expire_format',
        'details',
        'tax_rate',
        'tax_type',
        'tax_calc',
        'items',
        'sub_total',
        'sub_total_main',
        'sub_total_8',
        'tax',
        'tax_main',
        'tax_8',
        'total',
        'stamp',
        'remarks',
        'design',
    ];






    // /**
    //  * @return string
    //  */
    // static function createNumberFromFormat($number)
    // {
    //     $pattern1 = '/\[発行日:([ -~]*)\]/';
    //     if (preg_match($pattern1, $number, $matches1)) {
    //         $date_str = $matches1[1];
    //         $number = preg_replace($pattern1, date($date_str), $number);
    //     }
    //     return $number;
    // }

    /**
     * @return void
     */
    protected static function booted()
    {
        // データ作成時
        static::creating(function (GuestDocument $document) {
            $document->uuid = Str::uuid();
            return $document;
        });
    }
}

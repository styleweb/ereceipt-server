<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Customer extends Model
{
    use HasFactory;

    protected $casts = ['email' => 'json'];

    protected $fillable = [
        'name',
        'email',
        'group_id',
        'estimate_template_id',
        'delivery_template_id',
        'invoice_template_id',
        'receipt_template_id',
        'estimate_mail_template_id',
        'delivery_mail_template_id',
        'invoice_mail_template_id',
        'receipt_mail_template_id',
    ];

    protected static function booted()
    {
        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (Customer $customer) {
            $customer->uuid = Str::uuid();
            return $customer;
        });

        static::created(function (Customer $item) {
            $sequence = GroupSequence::firstOrCreate(['group_id' => $item->group_id], ['customer_sequence' => 0]);
            $sequence->increment('customer_sequence');
            $item->sequence = $sequence->customer_sequence;
            $item->save();
            return;
        });
    }

    public function invoices()
    {
        return $this->hasMany(Document::class);
    }
}

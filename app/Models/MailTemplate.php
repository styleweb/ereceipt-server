<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class MailTemplate extends Model
{
    use HasFactory;

    protected $casts = ['content' => 'json'];


    protected $fillable = [
        'group_id',
        'user_id',
        'customer_id',
        'type',
        'name',
        'from_name',
        'reply_to',
        'to_address',
        'cc',
        'bcc',
        'subject',
        'body',
        'content',
        'is_click_to_message_enabled',
        'is_html_mail_enabled',
        'before_body',
        'uses_mail_footer',
        'mail_footer',
        'is_contract',
    ];

    /**
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('own', function (Builder $builder) {
            $builder->where('group_id', Auth::user()->group_id);
        });

        // データ作成時
        static::creating(function (MailTemplate $item) {
            $item->uuid = Str::uuid();
            return $item;
        });

        static::created(function (MailTemplate $item) {
            // sequenceを追加
            $sequence = GroupSequence::firstOrCreate(['group_id' => $item->group_id], ['mail_template_sequence' => 0]);
            $sequence->increment('mail_template_sequence');
            $item->sequence = $sequence->mail_template_sequence;
            $item->save();
            return;
        });
    }
}

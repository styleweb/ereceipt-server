<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Document;
use FontLib\Table\Type\name;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class SetDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setDocument';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // if (!config('app.env') == 'local') {
        //     return;
        // }

        $documents = Document::withoutGlobalScope('own')->get();

        foreach ($documents as $item) {

            if ($item->contract_id !== 0) {
                $item->is_contract = 1;
            }
            $item->save();
        }

        return;
    }
}

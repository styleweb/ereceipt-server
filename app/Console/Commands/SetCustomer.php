<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use FontLib\Table\Type\name;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class SetCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setCustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // if (!config('app.env') == 'local') {
        //     return;
        // }

        // 対象となる契約を取得 
        $customers = Customer::withoutGlobalScope('own')->get();

        foreach ($customers as $customer) {

            $customer->email = [$customer->email2];
            $customer->save();
        }

        return;
    }
}

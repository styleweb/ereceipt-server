<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Models\Contract;
use App\Models\Mail;
use Illuminate\Support\Facades\Mail as Email;
use App\Mail\SendDocument;

class MailDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:mailDocument';

    protected $today;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->today = date('Y-m-d');
        if (config('app.env') == 'local') {
            $this->today = '2023-10-13';
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $documents = config('app.documents');

        foreach ($documents as $document) {

            // 対象となる契約を取得
            $contracts = Contract::withoutGlobalScope('own')
                ->where('state', 1)
                ->where('canceled', 0)
                ->where('is_auto_email', 1)
                ->where($document . '_id', '!=', 0)
                ->where($document . '_mail_dates', 'like', "%{$this->today}%")
                ->with('group')
                ->with(['customer' => function ($query) {
                    $query->withoutGlobalScope('own');
                }])
                ->with('estimate') // 帳票はﾒｰﾙﾃﾝﾌﾟﾚに複数のURLを入れられるよう、全部取得する
                ->with('delivery')
                ->with('invoice')
                ->with('receipt')
                ->with([$document . '_mail_template' => function ($query) {
                    $query->withoutGlobalScope('own');
                }])
                ->get();


            // 対象にメール送信
            foreach ($contracts as $contract) {

                // 帳票が作成されていない場合はスキップ
                if (!$contract->{$document}) {
                    continue;
                }

                // 以後送らない＆請求書にlogsがある場合はスキップ
                if ($contract->is_confirmation_email_disabled && !empty($contract->{$document}->logs)) {
                    continue;
                }

                // メールテンプレートがセットされていない場合はスキップ
                if (!$contract->{$document . '_mail_template'}) {
                    continue;
                }

                // メールテンプレートを取得
                $template = $contract->{$document . '_mail_template'};

                // URLを作成
                $site_url = config('app.site_url');
                $pdf_url = "{$site_url}/server/public/{$contract->{$document}->type}/{$contract->{$document}->uuid}";

                // メールテンプレートの内容置き換え

                $words = [
                    '/\{グループ名\}/' => $contract->group->name,
                    '/\{グループメールアドレス\}/'  => $contract->group->email,
                    '/\{宛名\}/' => $contract->{$document}->superscription,
                    '/\{敬称\}/'  => $contract->{$document}->attention,
                    '/\{契約名\}/'  => $contract->title,
                    '/\{開始日\}/' => $contract->start_date,
                    '/\{終了日\}/' => $contract->end_date,
                    '/\{有効期限\}/' => $contract->estimate ? $contract->estimate->expire_date : '',
                    '/\{支払期限\}/' => $contract->invoice ? $contract->invoice->expire_date : '',
                    '/\{PDFURL\}/' => $pdf_url,
                    '/\{見積書URL\}/' => $contract->estimate ? "{$site_url}/server/public/estimate/{$contract->estimate->uuid}" : '',
                    '/\{納品書URL\}/' => $contract->delivery ? "{$site_url}/server/public/delivery/{$contract->delivery->uuid}" : '',
                    '/\{請求書URL\}/' => $contract->invoice ? "{$site_url}/server/public/invoice/{$contract->invoice->uuid}" : '',
                    '/\{領収書URL\}/' => $contract->receipt ? "{$site_url}/server/public/receipt/{$contract->receipt->uuid}" : ''
                ];

                $pattern = array_keys($words);
                $replace = array_values($words);

                $template = $template->toArray();

                array_walk_recursive($template, function (&$value) use ($pattern, $replace) {
                    $value = preg_replace($pattern, $replace, $value);
                });

                $request = new Request;
                $request->merge([
                    'group_id' => $contract->group_id,
                    'document_id' => $contract->{$document . '_id'},
                    'from_name' => $template['from_name'],
                    'to_address' => 'info@styleweb.me',
                    'reply_to' => $template['reply_to'],
                    'subject' => $template['subject'],
                    'body' => $template['body']
                ]);

                // メールを保存
                $array = $request->toArray();
                Mail::create(
                    $array
                );

                // メールを送信
                Email::to('info@styleweb.me')->send(new SendDocument($request, $contract->group));
            }
        }
        return;
    }
}

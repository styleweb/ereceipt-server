<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Contract;
use App\Models\Document;
use App\Models\DocumentTemplate;

class MakeDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:makeDocument';

    protected $today;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->today = date('Y-m-d');
        // if (config('app.env') == 'local') {
        //     $this->today = '2024-10-02';
        // }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $documents = config('app.documents');
        foreach ($documents as $document) {

            // 作成日が今日の契約を取得
            $contracts = Contract::withoutGlobalScope('own')
                ->where('state', 1)
                ->where('canceled', 0)
                ->where('is_auto_document', 1)
                ->where($document . '_create_date', $this->today)
                ->with(['customer' => function ($query) {
                    $query->withoutGlobalScope('own');
                }])
                ->with('group')
                ->with('estimate')
                ->with('delivery')
                ->with('invoice')
                ->with('receipt')
                ->get();

            // 契約分だけ作成
            foreach ($contracts as $contract) {

                // 既に作成済みの帳票がある場合はスキップ
                if (!empty($contract->{$document})) {
                    continue;
                }

                // テンプレートを取得
                $template = DocumentTemplate::withoutGlobalScope('own')->find($contract->{$document . "_template_id"});

                // テンプレートが存在しない場合はスキップ
                if (!$template) {
                    continue;
                }

                // 発行日をセット
                $template->issue_date = $this->today;

                // uses_stamp=0はグループのスタンプを使用
                if (!$template->uses_stamp) {
                    $template->stamp = $contract->group->stamp;
                }
                // uses_remarks=0はグループの備考を使用
                if (!$template->uses_remarks) {
                    $template->remarks = $contract->group->remarks[$document];
                }

                // 支払期限をセット
                if (!empty($contract->{$document . "_expire_date"})) {
                    $template->expire_date = $contract->{$document . "_expire_date"};
                } else {
                    $template->expire_date = $this->setExpireDate($template->expire_calc_method, $template->expire_days);
                }

                // テンプレートのitemsの置き換え処理
                $replaced_item = $template->toJson(JSON_UNESCAPED_UNICODE);

                $pattern = ['/\{契約名\}/', '/\{開始日\}/', '/\{終了日\}/', '/\{次回開始日\}/', '/\{次回終了日\}/', '/\{支払期限\}/', '/\{顧客名\}/'];
                $replace = [$contract->title, $contract->start_date, $contract->end_date, $contract->next_start_date, $contract->next_end_date, $template->expire_date, $contract->customer->name];

                $replaced_item = preg_replace($pattern, $replace, $replaced_item);
                $replaced_item = json_decode($replaced_item, true);

                // 契約情報から請求情報に値をセット
                $set_data = [
                    'group_id' => $contract->group_id,
                    'contract_id' => $contract->id,
                    'customer_id' => $contract->customer_id,
                    'superscription' => $contract->customer->name,
                ];

                // 同一Customerに保留の請求がある場合は合算
                $pending_invoices = Document::withoutGlobalScope('own')->where('customer_id', $contract->customer_id)->where('type', 'invoice')->where('state', 3)->where('parent_id', 0)->get();

                if (!empty($pending_invoices)) {
                    foreach ($pending_invoices as $pending_invoice) {
                        $replaced_item['sub_total'] += $pending_invoice->sub_total;
                        $replaced_item['tax'] += $pending_invoice->tax;
                        $replaced_item['total'] += $pending_invoice->total;
                    }
                }
                $set_data['children'] = $pending_invoices->toArray();


                // 書類作成
                $new_document = Document::withoutGlobalScope('own')->create(
                    array_merge(
                        $replaced_item,
                        $set_data
                    )
                );

                // 保留の請求を合算した場合は、parent_idをセット
                if (!empty($pending_invoices)) {
                    foreach ($pending_invoices as $pending_invoice) {
                        $pending_invoice->parent_id = $new_document->id;
                        $pending_invoice->save();
                    }
                }
            }
        }

        return;
    }

    public function setExpireDate($expireMethodType, $expireDays)
    {
        switch ($expireMethodType) {
            case 10: // 発行日から〇日
                $year = date('Y', strtotime($this->today));
                $month = date('m', strtotime($this->today));
                $day = date('d', strtotime($this->today));
                return date('Y-m-d', strtotime("{$year}-{$month}-{$day} {$expireDays} day"));
                break;

            case 20: // 翌月指定日
                $year = date('Y', strtotime($this->today));
                $month = date('m', strtotime($this->today));
                $day = date('d', strtotime($this->today));
                return date('Y-m-d', strtotime("{$year}-{$month}-{$expireDays} 1 month"));
                break;

            case 30: // 翌々月指定日
                $year = date('Y', strtotime($this->today));
                $month = date('m', strtotime($this->today));
                $day = date('d', strtotime($this->today));
                return date('Y-m-d', strtotime("{$year}-{$month}-{$expireDays} 1 month"));
                break;

            case 35: // 当月末
                $year = date('Y', strtotime($this->today));
                $month = date('m', strtotime($this->today));
                $day = date('d', strtotime($this->today));
                return date('Y-m-d', strtotime("{$year}-{$month}-01 +1 month -1 day"));
                break;

            case 40: // 翌月末
                $year = date('Y', strtotime($this->today));
                $month = date('m', strtotime($this->today));
                $day = date('d', strtotime($this->today));
                return date('Y-m-d', strtotime("{$year}-{$month}-01 +2 month -1 day"));
                break;

            case 50: // 翌々月末
                $year = date('Y', strtotime($this->today));
                $month = date('m', strtotime($this->today));
                $day = date('d', strtotime($this->today));
                return date('Y-m-d', strtotime("{$year}-{$month}-01 +3 month -1 day"));
                break;
            default:
                # code...
                break;
        }
    }
}

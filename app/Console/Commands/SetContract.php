<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Contract;
use App\Models\DocumentTemplate;
use FontLib\Table\Type\name;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class SetContract extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setContract';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!config('app.env') == 'local') {
            return;
        }

        // 対象となる契約を取得 
        $contracts = Contract::where('state', 1)->where('group_id', 1)->where('amount', 34000)->get();

        foreach ($contracts as $contract) {

            // // uuid
            // $contract->uuid = Str::uuid();


            // // 請求書作成日
            // $contract->invoice_create_date = date('Y-m-d', strtotime("{$contract->payment_date} -65 day"));


            // // 次年度利用期間
            // $contract->next_start_date = date('Y-m-d', strtotime("{$contract->start_date} +1 year"));
            // $contract->next_end_date = date('Y-m-d', strtotime("{$contract->end_date} +1 year"));


            // // メール送信日
            // $sends = [-60, -30, -10, -5, -1];
            // $mail_dates = [];
            // foreach ($sends as $send) {
            //     $mail_dates[] = date('Y-m-d', strtotime("{$contract->payment_date} {$send} day"));
            // }
            // $contract->mail_dates = $mail_dates;


            // 請求書テンプレート
            $template = [
                'name' => $contract->title . '用テンプレート',
                'title' => '請求書',
                'text' => '下記の通りご請求申し上げます。',
                'group_id' => 1,
                'type' => 'invoice',
                'expire_days' => 65,
                'details' => [["title" => "件名", "content" => $contract->title]],
                'items' => [
                    [
                        "title" => "{契約名}",
                        "detail_enable" => true,
                        "detail" => "{開始日}～{終了日}までの利用料金",
                        "quantity" => 1,
                        "unit" => "",
                        "price" => 27000,
                        "amount" => 27000,
                        "tax_rate" => 10
                    ]
                ],
                'tax_rate' => 10,
                'tax_type' => 2,
                'tax_calc' => 2,
                'sub_total' => 27000,
                'tax' => 2700,
                'total' => 29700,
                'is_contract' => 1,
            ];


            switch ($contract->amount) {
                    // case 27000:
                    //     $contract->invoice_template_id = 2;
                    //     break;

                    // case 31000:
                    //     $contract->invoice_template_id = 4;
                    //     break;

                case 34000:
                    $contract->invoice_template_id = 8;
                    break;

                    // default:
                    //     $new_template = [
                    //         'items' => [
                    //             [
                    //                 "title" => "{契約名}",
                    //                 "detail_enable" => true,
                    //                 "detail" => "{開始日}～{終了日}までの利用料金",
                    //                 "quantity" => 1,
                    //                 "unit" => "",
                    //                 "price" => $contract->amount,
                    //                 "amount" => $contract->amount,
                    //                 "tax_rate" => 10
                    //             ]
                    //         ],
                    //         'sub_total' => $contract->amount,
                    //         'tax' => $contract->amount * 0.1,
                    //         'total' => $contract->amount + ($contract->amount * 0.1),
                    //     ];
                    //     // templateにマージ
                    //     $mergeTemplate = array_merge($template, $new_template);

                    //     $resultTemplate = DocumentTemplate::create(
                    //         $mergeTemplate
                    //     );

                    //     $contract->invoice_template_id = $resultTemplate->id;
                    //     break;
            }


            // // メールテンプレート
            // $contract->mail_template_id = 4;


            // $contract->state = 1;


            $contract->save();
        }

        return;
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {

            // 見積、請求、共通
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('group_id')->default(0);
            $table->foreignId('user_id')->default(0);
            $table->foreignId('customer_id')->nullable();

            $table->string('title')->nullable();
            $table->string('text')->nullable();

            $table->string('superscription')->nullable();
            $table->string('attention')->nullable();

            $table->string('number')->nullable();

            $table->string('issue_date')->nullable();
            $table->string('issue_format')->default('YYYY年M月D日');

            $table->string('expire_date')->nullable();
            $table->smallInteger('expire_type')->default(2);
            $table->smallInteger('expire_calc_method')->default(10);
            $table->integer('expire_days')->default(14);
            $table->string('expire_format')->default('YYYY年M月D日(dd)');
            $table->string('expire_text')->nullable();

            $table->json('details')->default(json_encode([['title' => '', 'content' => '']], JSON_UNESCAPED_UNICODE));

            $table->integer('tax_rate')->default(10);
            $table->smallInteger('tax_type')->default(2);
            $table->smallInteger('tax_calc')->default(2);

            $table->json('items')->default(json_encode([['title' => '', 'detail_enable' => false, 'detail' => '', 'quantity' => 1, 'unit' => '', 'price' => 0, 'amount' => 0, 'tax_rate' => 10]], JSON_UNESCAPED_UNICODE));

            $table->integer('sub_total')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('total')->default(0);

            $table->json('stamp')->default(json_encode(
                [
                    'stamp_name' => '',
                    'stamp_type' => 0,
                    'stamp_shape' => 0,
                    'details'  => [['title' => '', 'content' => '']],
                    'stamp_image_path'  => '',
                    'company_image_path'  => ''
                ],
                JSON_UNESCAPED_UNICODE
            ));

            $table->json('remarks')->default(json_encode([['title' => '', 'content' => '']], JSON_UNESCAPED_UNICODE));

            $table->text('memo')->nullable();
            $table->string('design')->nullable();
            $table->smallInteger('state')->default(0);
            $table->tinyInteger('enabled')->default(1);
            $table->json('logs')->default('[]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimates');
    }
}

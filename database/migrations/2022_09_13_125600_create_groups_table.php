<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('groups', function (Blueprint $table) {

            $documents = ['estimate', 'invoice', 'delivery', 'receipt'];


            $table->id();
            $table->string('name')->default('');
            // $table->string('credit_customer_id')->nullable();
            // $table->string('zipcode')->default('');
            // $table->string('address')->default('');
            // $table->string('address2')->default('');
            // $table->string('tel')->default('');
            // $table->string('fax')->default('');
            // $table->string('email')->default('');
            // $table->string('web')->default('');
            // $table->string('invoice_number')->default('');

            // $table->string('mail_from_address')->nullable();
            // $table->string('mail_host')->nullable();
            // $table->string('mail_port')->nullable();
            // $table->string('mail_user_name')->nullable();
            // $table->string('mail_password')->nullable();
            // $table->string('mail_encryption')->nullable();

            foreach ($documents as $document) {
                $template[$document] = 0;
            }
            $table->json('template_default')->default(json_encode($template));


            // $table->json('estimate_mail_template')->default(json_encode(
            //     [
            //         'subject' => '',
            //         'body' => ''
            //     ]
            // ));
            // $table->json('invoice_mail_template')->default(json_encode(
            //     [
            //         'subject' => '',
            //         'body' => ''
            //     ]
            // ));
            // $table->json('delivery_mail_template')->default(json_encode(
            //     [
            //         'subject' => '',
            //         'body' => ''
            //     ]
            // ));
            // $table->json('receipt_mail_template')->default(json_encode(
            //     [
            //         'subject' => '',
            //         'body' => ''
            //     ]
            // ));
            foreach ($documents as $document) {
                $mail_template[$document] = 0;
            }
            $table->json('mail_template_default')->default(json_encode($mail_template));


            foreach ($documents as $document) {
                $numbering[$document] =
                    [
                        'total' => 0,
                        'year' => ['date' => '', 'count' => 0],
                        'month' => ['date' => '', 'count' => 0],
                        'day'  => ['date' => '', 'count' => 0]
                    ];
            }
            $table->json('numbering')->default(json_encode($numbering));


            $table->json('states')->default('');

            $table->json('default_queries')->default('');

            // $table->json('invoice_numbering')->default(json_encode(
            //     [
            //         'total' => 0,
            //         'year' => ['date' => '', 'count' => 0],
            //         'month' => ['date' => '', 'count' => 0],
            //         'day'  => ['date' => '', 'count' => 0]
            //     ]
            // ));
            // $table->json('delivery_numbering')->default(json_encode(
            //     [
            //         'total' => 0,
            //         'year' => ['date' => '', 'count' => 0],
            //         'month' => ['date' => '', 'count' => 0],
            //         'day'  => ['date' => '', 'count' => 0]
            //     ]
            // ));
            // $table->json('receipt_numbering')->default(json_encode(
            //     [
            //         'total' => 0,
            //         'year' => ['date' => '', 'count' => 0],
            //         'month' => ['date' => '', 'count' => 0],
            //         'day'  => ['date' => '', 'count' => 0]
            //     ]
            // ));
            $table->json('stamp')->default(json_encode(
                [
                    'company_image_path'  => '',
                    'details'  => [['title' => '', 'content' => '']],
                    'stamp_type' => 0,
                    'stamp_name' => '',
                    'stamp_shape' => 0,
                    'stamp_image_path'  => '',
                ],
                JSON_UNESCAPED_UNICODE
            ));
            $table->tinyInteger('stamp_template_enabled')->default(0);
            $table->tinyInteger('paused')->default(0);
            // $table->tinyInteger('has_subscription')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}

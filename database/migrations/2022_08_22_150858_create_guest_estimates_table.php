<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_estimates', function (Blueprint $table) {
            $table->id();
            $table->string('superscription')->nullable();
            $table->string('attention')->nullable();
            $table->string('number')->nullable();
            $table->string('issue_date')->nullable();
            $table->string('expire_date')->nullable();
            $table->json('items')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('tax')->nullable();

            $table->string('total')->nullable();
            $table->text('information')->nullable();
            $table->tinyInteger('stamp_type')->default(1);
            $table->tinyInteger('stamp_shape')->default(3);
            $table->string('stamp_name')->nullable();
            $table->string('stamp_image')->nullable();
            $table->string('company_image')->nullable();
            $table->json('remarks')->nullable();
            $table->string('uuid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_estimates');
    }
}

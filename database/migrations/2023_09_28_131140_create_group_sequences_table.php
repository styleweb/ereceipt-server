<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupSequencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_sequences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('group_id')->constrained()->onDelete('cascade');
            $table->integer('contract_sequence')->default(1);
            $table->integer('customer_sequence')->default(1);
            $table->integer('document_sequence')->default(1);
            $table->integer('document_template_sequence')->default(1);
            $table->integer('file_sequence')->default(1);
            $table->integer('mail_sequence')->default(1);
            $table->integer('mail_template_sequence')->default(1);
            $table->integer('user_sequence')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_sequences');
    }
}

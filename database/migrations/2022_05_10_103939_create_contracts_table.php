<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {

            // 見積、請求、共通
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('group_id')->default(1); // あとでゼロにする。一時的。ＤＢ取り込み用
            $table->foreignId('user_id')->default(0);
            $table->foreignId('customer_id')->default(0);
            $table->foreignId('mail_template_id')->default(0);
            $table->foreignId('invoice_template_id')->default(0);
            $table->foreignId('estimate_id')->default(0);
            $table->foreignId('invoice_id')->default(0);
            $table->foreignId('delivery_id')->default(0);
            $table->foreignId('receipt_id')->default(0);
            $table->date('invoice_create_date')->nullable();
            $table->json('invoice_mail_dates')->default(json_encode([], JSON_UNESCAPED_UNICODE));

            $invoice_mail_cycles = [
                ['unit' => 'day', 'value' => -60],
                ['unit' => 'day', 'value' => -30],
                ['unit' => 'day', 'value' => -14],
                ['unit' => 'day', 'value' => -7],
                ['unit' => 'day', 'value' => -1],
            ];
            $table->json('invoice_mail_cycles')->default(json_encode([$invoice_mail_cycles], JSON_UNESCAPED_UNICODE));

            $table->string('title')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();

            $table->string('cycle_unit')->default('year');
            $table->smallInteger('cycle_value')->default(1);

            $table->string('payment_date_cycle_unit')->default('month');
            $table->smallInteger('payment_date_cycle_value')->default(1);

            $table->string('invoice_create_date_cycle_unit')->default('month');
            $table->smallInteger('invoice_create__date_cycle_value')->default(1);

            $table->text('memo')->nullable();
            $table->smallInteger('state')->default(0);
            $table->tinyInteger('auto')->default(1);
            $table->tinyInteger('cancel')->default(1);
            $table->tinyInteger('enabled')->default(1);
            $table->timestamps();

            // 後で消す
            $table->string('payment_date')->nullable();
            $table->integer('amount')->default(0);
            $table->text('detail')->nullable();
            $table->smallInteger('status')->default(1);
            $table->string('created')->nullable();
            $table->string('modified')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}

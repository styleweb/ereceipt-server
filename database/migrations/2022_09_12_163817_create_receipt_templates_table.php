<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_templates', function (Blueprint $table) {

            $table->string('name')->nullable();

            $table->string('title')->nullable();
            $table->string('text')->nullable();

            // 見積、請求、共通
            $table->id();
            $table->foreignId('group_id')->default(0);
            $table->foreignId('user_id')->default(0);
            $table->foreignId('customer_id')->nullable();

            $table->string('superscription')->nullable();
            $table->string('attention')->nullable();

            $table->string('issue_format')->default('YYYY年M月D日');

            $table->json('details')->default(json_encode([['title' => '', 'content' => '']]));

            $table->integer('tax_rate')->default(10);
            $table->smallInteger('tax_type')->default(2);
            $table->smallInteger('tax_calc')->default(2);

            $table->json('items')->default(json_encode([['title' => '', 'quantity' => '', 'unit' => '', 'price' => '', 'amount' => '']]));
            $table->integer('sub_total')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('total')->default(0);

            $table->json('remarks')->default(json_encode([['title' => '', 'content' => '']]));

            $table->string('design')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_templates');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            // 見積、請求、共通
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('group_id')->default(0);
            $table->foreignId('user_id')->default(0);
            $table->foreignId('customer_id')->nullable();

            $table->string('title')->nullable();
            $table->string('text')->nullable();

            $table->string('superscription')->nullable();
            $table->string('attention')->nullable();

            $table->string('number')->nullable();
            $table->string('number_format')->nullable();
            $table->smallInteger('number_type')->default(1);

            $table->string('issue_date')->nullable();
            $table->string('issue_format')->default('YYYY年M月D日');

            $table->string('expire_date')->nullable();
            $table->smallInteger('expire_type')->default(2);
            $table->smallInteger('expire_calc_method')->default(10);
            $table->integer('expire_days')->default(14);
            $table->string('expire_format')->default('YYYY年M月D日(dd)');
            $table->string('expire_text')->nullable();

            $table->json('details')->default(json_encode([['title' => '', 'content' => '']]));

            $table->integer('tax_rate')->default(10);
            $table->smallInteger('tax_type')->default(2);
            $table->smallInteger('tax_calc')->default(2);
            $table->json('items')->default(json_encode([['title' => '', 'quantity' => '', 'unit' => '', 'price' => '', 'amount' => '']]));
            $table->integer('sub_total')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('total')->default(0);

            $table->smallInteger('stamp_type')->default(1);
            $table->text('information')->nullable();
            $table->smallInteger('stamp_shape')->default(3);
            $table->string('stamp_name')->nullable();
            $table->string('stamp_image')->nullable();
            $table->string('company_image')->nullable();
            $table->json('remarks')->default(json_encode([['title' => '', 'content' => '']]));

            $table->string('design')->nullable();
            $table->tinyInteger('enabled')->default(1);
            $table->json('logs')->default('[]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}

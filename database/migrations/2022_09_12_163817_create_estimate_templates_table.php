<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_templates', function (Blueprint $table) {

            // ここからテンプレートのみ
            $table->string('name')->default('名称未設定');
            $table->string('title')->default('見積書');
            $table->string('text')->default('下記の通りお見積り申し上げます。');

            // 見積、請求、共通
            $table->id();
            $table->foreignId('group_id')->default(0);
            $table->foreignId('user_id')->default(0);
            $table->foreignId('customer_id')->nullable();

            $table->string('superscription')->nullable();
            $table->string('attention')->default('御中');

            $table->string('number')->default('{発行日:ymd}-{連番:月,3}');

            $table->string('issue_format')->default('YYYY年M月D日');

            $table->string('expire_format')->default('YYYY年M月D日(dd)');
            $table->smallInteger('expire_type')->default(2);
            $table->smallInteger('expire_calc_method')->default(10);
            $table->integer('expire_days')->default(14);

            $table->json('details')->default(json_encode([['title' => '', 'content' => '']], JSON_UNESCAPED_UNICODE));

            $table->integer('tax_rate')->default(10);
            $table->smallInteger('tax_type')->default(2);
            $table->smallInteger('tax_calc')->default(2);
            $table->json('items')->default(json_encode([['title' => '', 'detail_enable' => false, 'detail' => '', 'quantity' => 1, 'unit' => '', 'price' => 0, 'amount' => 0]], JSON_UNESCAPED_UNICODE));
            $table->integer('sub_total')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('total')->default(0);

            $table->json('remarks')->default(json_encode([['title' => '', 'content' => '']], JSON_UNESCAPED_UNICODE));

            $table->string('design')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_templates');
    }
}

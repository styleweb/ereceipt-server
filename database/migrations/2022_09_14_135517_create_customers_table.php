<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('group_id')->default(1); // あとでゼロにする。一時的。ＤＢ取り込み用
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->string('person_in_charge')->nullable();
            $table->string('email')->nullable();
            $table->text('memo')->nullable();
            $table->timestamps();

            // 後で消す
            $table->string('name_kana')->nullable();
            $table->string('email_before')->nullable();
            $table->smallInteger('payment_method')->nullable();
            $table->string('postcode')->nullable();
            $table->string('prefecture')->nullable();
            $table->string('address')->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('mobile')->nullable();
            $table->string('staff_name_last')->nullable();
            $table->string('staff_name_first')->nullable();
            $table->string('staff_name_last_kana')->nullable();
            $table->string('staff_name_first_kana')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('created')->nullable();
            $table->string('modified')->nullable();
            $table->smallInteger('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

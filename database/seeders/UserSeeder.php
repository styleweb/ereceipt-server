<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $invoice_body = <<<EOM
{宛名} {敬称}

いつもお世話になっております。
請求書をご案内させていただきます。
ご査収の程よろしくお願いいたします。
=======================================
◎請求書
{PDFURL}
=======================================

ご不明な点等ございましたらお気軽にお問い合わせください。
よろしくお願い致します。

…━━━…‥・・・‥…━━━…・・‥…
{グループ名}
担当者: {ユーザー名}
EMAIL: {メールアドレス}
…━━━…‥・・・‥…━━━…・・‥…
EOM;

        $remark_bank = <<<EOM
楽天銀行　ドラム支店(213) 普通 5003665 スタイルウェブ　ミヤザト　ダイ
※振込手数料はお客様ご負担でお願いします。
EOM;

        $contract_mail = <<<EOM
{宛名} {敬称}

※このメールは自動配信システムにより送信されています。

いつもスタイルウェブをご利用いただきまして誠に有難うございます。

さて、現在ご利用中のドメインとレンタルサーバーが更新期限日の-21日前となりましたので、
ご案内申し上げます。

つきましては、下記のご利用規約をご一読の上、請求書記載の期限までにお振込いただきますよう、お願い申し上げます。


◎現在のご契約
=======================================
【 内　容 】 sankyoo.co.jp ウェブホスティング契約　
【 期　間 】 2022-04-22～2023-04-21　
=======================================

◎請求書
=======================================
ご請求書
{PDFURL}

ご利用規約
http://www.ryukyunet.biz/images/rules/hosting.pdf
=======================================

◎更新期限日（入金締切日）
=======================================
2023-06-30（金）15:00
=======================================

◎ご注意事項
=======================================
・お払込みがないまま期間を経過しますと、ホームページのデータは全て削除され、
メールなどの関連サービスも全て停止します。
予めご了承ください。

・ご解約について
解約をご希望の場合は、お手数ですがメールにて必ずご連絡ください。
=======================================

以上、ご不明な点等ございましたらお気軽にお問い合わせください。

何卒、宜しくお願い申し上げます。

…━━━…‥・・・‥…━━━…・・‥…
{グループ名}
担当者: {ユーザー名}
EMAIL: {メールアドレス}
…━━━…‥・・・‥…━━━…・・‥…
EOM;


        $users = [
            ['name' => '宮里', 'email' => 'miyazato@styleweb.me']
        ];
        foreach ($users as $user) {
            DB::table('users')->insert([
                'group_id' => 1,
                'name' => $user['name'],
                'email' => $user['email'],
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => Str::random(10),
                'admin_enable' => 1,
            ]);

            DB::table('groups')->insert([
                'name' => 'スタイルウェブ',

                // 'mail_from_address' => 'test@sws02.xsrv.jp',
                // 'mail_host' => 'sv5104.xserver.jp',
                // 'mail_port' => '465',
                // 'mail_user_name' => 'test@sws02.xsrv.jp',
                // 'mail_password' => 'i2sJQ3_m-F4XAYT',
                // 'mail_encryption' => 'ssl',

                'template_default' => json_encode(
                    ['invoice' => 1, 'estimate' => 1, 'delivery' => 1, 'receipt' => 1]
                ),
                'mail_template_default' => json_encode(
                    ['invoice' => 1, 'estimate' => 1, 'delivery' => 1, 'receipt' => 1, 'contract' => 1]
                ),

                'states' => json_encode(
                    [
                        'estimate' => [
                            ['id' => 1, 'name' => '未送信', 'default' => true],
                            ['id' => 2, 'name' => '送信済', 'default' => false],
                            ['id' => 3, 'name' => '受注', 'default' => false],
                            ['id' => 4, 'name' => '失注', 'default' => false],
                        ],
                        'invoice' => [
                            ['id' => 1, 'name' => '請求中', 'default' => true],
                            ['id' => 2, 'name' => '入金済', 'default' => false],
                            ['id' => 3, 'name' => '保留', 'default' => false],
                        ],
                    ]
                ),

                'stamp' => json_encode(
                    [
                        'company_image_path'  => 'file/styleweb.png',
                        'details'  => [
                            ['title' => '', 'content' => ''],
                            // ['title' => '', 'content' => 'スタイルウェブ'],
                            // ['title' => '', 'content' => '〒901-1304'],
                            // ['title' => '', 'content' => '沖縄県島尻郡与那原町字東浜14-7'],
                            // ['title' => 'TEL', 'content' => '050-3177-2100'],
                            // ['title' => 'MAIL', 'content' => 'info@styleweb.me'],
                            // ['title' => 'web', 'content' => 'https://www.ryukyunet.biz/'],
                        ],
                        'stamp_type' => 0,
                        'stamp_name' => '',
                        'stamp_shape' => 1,
                        // 'stamp_type' => 1,
                        // 'stamp_name' => 'スタイルウェブ',
                        // 'stamp_shape' => 2,
                        'stamp_image_path'  => '',
                    ],
                    JSON_UNESCAPED_UNICODE
                ),
            ]);

            DB::table('files')->insert(
                [
                    [
                        'group_id' => 1,
                        'name' => 'styleweb.png',
                        'path' => 'file/styleweb.png',
                        'size' => '17766',
                        'extension' => 'png',
                    ],
                    [
                        'group_id' => 1,
                        'name' => 'stylewebservice.png',
                        'path' => 'file/stylewebservice.png',
                        'size' => '38875',
                        'extension' => 'png',
                    ]
                ]
            );

            DB::table('invoice_templates')->insert([
                'group_id' => 1,
                'user_id' => 1,

                // 'name' => 'デフォルトテンプレート',
                // 'attention' => '御中',

                // 'number' => '[発行日:Ymd]-[連番:day,3]',
                // 'issue_format' => 'YYYY年M月D日',

                // 'expire_type' => 2,
                // 'expire_format' => 'YYYY年M月D日(dd)',
                // 'expire_calc_method' => 10,
                // 'expire_days' => 14,

                // 'details' => json_encode(
                //     [
                //         [
                //             'title' => '件名',
                //             'content' => ''
                //         ]
                //     ]
                // ),

                'remarks' => json_encode(
                    [
                        [
                            'title' => 'お振込み先',
                            'content' => $remark_bank
                        ]
                    ]
                ),
            ]);

            // DB::table('contracts')->insert([
            //     'group_id' => 1,
            //     'user_id' => 1,
            //     'customer_id' => 1,
            //     'uuid' => 'hfoahfoeireorjelkazbvg4343dawe4334',
            //     'title' => '****ウェブホスティング契約',
            //     'start_date' => '2022-04-22',
            //     'end_date' => '2023-04-21',
            //     'expire_date' => '2023-10-30',
            // ]);

            DB::table('mail_templates')->insert([
                'group_id' => 1,
                'user_id' => 1,
                'type' => 'invoice',
                'name' => 'デフォルトテンプレート',
                'subject' => '【御請求書案内】{グループ名}',
                'body' => $invoice_body
            ]);

            // DB::table('customers')->insert([
            //     [
            //         'group_id' => 1,
            //         'name' => '株式会社花樹園',
            //         'person_in_charge' => '秋元',
            //         'email' => 'info@styleweb.me',
            //     ],
            //     [
            //         'group_id' => 1,
            //         'name' => 'スタイルウェブ',
            //         'person_in_charge' => '宮里',
            //         'email' => 'miyazato@styleweb.me',
            //     ],
            //     [
            //         'group_id' => 1,
            //         'name' => '琉球大学長',
            //         'person_in_charge' => '上原',
            //         'email' => 'info@styleweb.me',
            //     ],
            //     [
            //         'group_id' => 1,
            //         'name' => '玉城龍二',
            //         'person_in_charge' => '',
            //         'email' => 'info@styleweb.me',
            //     ],
            // ]);
        }
    }
}
